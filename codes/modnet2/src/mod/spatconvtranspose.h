#ifndef SPATCONVTRANSPOSE_H
#define SPATCONVTRANSPOSE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "types.h"
#include "soc_drivers.h"

typedef struct SpatconvTranspose* SPATCONVTRANSPOSE;

SPATCONVTRANSPOSE spatconvtranspose_create(void);
RET spatconvtranspose_forward_wrap(SPATCONVTRANSPOSE sc);
RET spatconvtranspose_init(SPATCONVTRANSPOSE sc, NAME weightsFile, NAME biasFile, SIZE pin,
		SIZE pout, SIZE kern_s[2], DATA** wPointer);

RET spatconvtranspose_forward_sw(SPATCONVTRANSPOSE sc, DATA* input, DATA* output, SIZE in_s[3],
                     	SIZE out_s[3], SIZE stride[2], SIZE pad[2], bool activate = false, int ncol=4, int qf=10, int precision8=0);




RET spatconvtranspose_destroy(SPATCONVTRANSPOSE sc);

RET spatconvtranspose_sw_8(DATA* input, DATA* output, DATA* kernel,
		DATA* bias, SIZE in_s[3], SIZE out_s[3], SIZE kern_s[2], SIZE dil[2], SIZE pad[2],
		SIZE stride[2], bool activate = false, int ncol=4, int qf=4);

RET spatconvtranspose_sw_16(DATA* input, DATA* output, DATA* kernel,
		DATA* bias, SIZE in_s[3], SIZE out_s[3], SIZE kern_s[2], SIZE dil[2], SIZE pad[2],
		SIZE stride[2], bool activate = false, int ncol=4, int qf=4);



struct SpatconvTranspose {
	DATA* kernel;
	DATA* bias;
	VARSIZE pin;
	VARSIZE pout;
	VARSIZE kern_s[4];
	VARSIZE maxog;
	VARSIZE dil[2];

    DATA* input;
    DATA* output;
    
    VARSIZE in_s[3];
    VARSIZE out_s[3];
    VARSIZE stride[2];
    VARSIZE pad[2];
    bool activate;
    int qf;
    int precision8;
};


#ifdef __cplusplus
}
#endif
#endif
