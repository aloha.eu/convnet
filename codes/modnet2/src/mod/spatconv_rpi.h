#ifndef SPATCONV_RPI_H
#define SPATCONV_RPI_H

#ifdef __cplusplus
extern "C" {
#endif

#include "types.h"
#include "soc_drivers.h"

typedef float DATARPI;

typedef struct Spatconv_rpi* SPATCONV_RPI;
typedef struct Spatconv_rpi_f* SPATCONV_RPI_F;

SPATCONV_RPI spatconv_rpi_create(void);
RET spatconv_rpi_forward_wrap(SPATCONV_RPI sc);

RET spatconv_rpi_forward_sw(SPATCONV_RPI sc, DATA* input, DATA* output, SIZE in_s[3],
                     	SIZE out_s[3], SIZE stride[2], SIZE pad[2],
                      DATARPI *scalers_bn, DATARPI *biases_bn, DATARPI num, DATARPI scaler, DATARPI divider, DATARPI clip_max, DATARPI clip_min, 
                      bool activate = false, int ncol=4, int groups=1, int qf=0, int precision8=0);

RET spatconv_rpi_destroy(SPATCONV_RPI sc);

RET spatconv_rpi_sw_16(DATA* input, DATA* output, DATA* kernel,
    DATA* bias, SIZE in_s[3], SIZE out_s[3], SIZE kern_s[2], SIZE dil[2], SIZE pad[2], SIZE stride[2],
    DATARPI *scalers_bn, DATARPI *biases_bn, DATARPI num, DATARPI scaler, DATARPI divider, DATARPI clip_max, DATARPI clip_min,
    bool activate = false, int ncol=4, int groups=1, int qf=4);



SPATCONV_RPI_F spatconv_rpi_f_create(void);
RET spatconv_rpi_f_forward_wrap(SPATCONV_RPI_F sc);

RET spatconv_rpi_f_forward_sw(SPATCONV_RPI_F sc, DATA* input, DATARPI* output, SIZE in_s[3],
                     	SIZE out_s[3], SIZE stride[2], SIZE pad[2],
                      DATARPI *scalers_bn, DATARPI *biases_bn, DATARPI num, DATARPI scaler, DATARPI divider, DATARPI clip_max, DATARPI clip_min, 
                      bool activate = false, int ncol=4, int groups=1, int qf=0, int precision8=0);

RET spatconv_rpi_f_destroy(SPATCONV_RPI_F sc);

RET spatconv_rpi_f_sw_16(DATA* input, DATARPI* output, DATA* kernel,
    DATA* bias, SIZE in_s[3], SIZE out_s[3], SIZE kern_s[2], SIZE dil[2], SIZE pad[2], SIZE stride[2],
    DATARPI *scalers_bn, DATARPI *biases_bn, DATARPI num, DATARPI scaler, DATARPI divider, DATARPI clip_max, DATARPI clip_min,
    bool activate = false, int ncol=4, int groups=1, int qf=4);



//RET spatconv_rpi_init(SPATCONV_RPI sc, NAME weightsFile, NAME biasFile, SIZE pin, SIZE pout, SIZE kern_s[2], DATA** wPointer);

struct Spatconv_rpi {
    DATA* kernel;
    DATA* bias;
    VARSIZE pin;
    VARSIZE pout;
    VARSIZE kern_s[4];
    VARSIZE maxog;
    VARSIZE dil[2];

    DATA* input;
    DATA* output;
    
    
    DATARPI* scalers_bn;
    DATARPI* biases_bn;
    DATARPI num;
    
    DATARPI scaler;
    DATARPI divider;
    
    DATARPI clip_max;
    DATARPI clip_min;
    
    VARSIZE in_s[3];
    VARSIZE out_s[3];
    VARSIZE stride[2];
    VARSIZE pad[2];
    int groups;
    bool activate;// not used
    int qf; // not used
    int precision8;// not used
};

struct Spatconv_rpi_f {
    DATA* kernel;
    DATA* bias;
    VARSIZE pin;
    VARSIZE pout;
    VARSIZE kern_s[4];
    VARSIZE maxog;
    VARSIZE dil[2];

    DATA* input;
    DATARPI* output;
    
    
    DATARPI* scalers_bn;
    DATARPI* biases_bn;
    DATARPI num;
    
    DATARPI scaler;
    DATARPI divider;
    
    DATARPI clip_max;
    DATARPI clip_min;
    
    VARSIZE in_s[3];
    VARSIZE out_s[3];
    VARSIZE stride[2];
    VARSIZE pad[2];
    int groups;
    bool activate;// not used
    int qf; // not used
    int precision8;// not used
};


#ifdef __cplusplus
}
#endif
#endif
