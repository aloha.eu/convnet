#ifndef OPERATORS_H
#define OPERATORS_H

#include "types.h"

struct Mul {
	DATA* input;
	DATA* output;
	int size;
	DATA* scaler;
	int num;
	
	int qf;
};

typedef struct Mul* MUL;

struct Mul_f {
	float* input;
	float* output;
	int size;
	float* scaler;
	int num;
	
	int qf;
};

typedef struct Mul_f* MUL_F;

struct Div {
	DATA* input;
	DATA* output;
	int size;
	DATA* divider;
	int num;
	
	int qf;
};

typedef struct Div* DIV;

struct Div_f {
	float* input;
	float* output;
	int size;
	float* divider;
	int num;
	
	int qf;
};

typedef struct Div_f* DIV_F;


struct Add {
	DATA* input;
	DATA* output;
	int size;
	
	DATA* bias;
	int num;
};

typedef struct Add* ADD;


struct Add_f {
	float* input;
	float* output;
	int size;
	
	float* bias;
	int num;
};

typedef struct Add_f* ADD_F;


struct Shift {
	DATA* input;
	DATA* output;
	int size;
	
	int bit;
	int dir; // 0 dx, 1 sx
	int qf;
};

typedef struct Shift* SHIFT;

struct Sigmoid {
	DATA* input;
	DATA* output;
	int size;
	int qf;
};

typedef struct Sigmoid* SIGMOID;

struct Input {
	DATA* output;
	int  size[3];

};

typedef struct Input* INPUT;

struct Input_f {
	float* output;
	int  size[3];

};

typedef struct Input_f* INPUT_F;

struct Concat {
	DATA** input;
	DATA* output;
	int*  size;
	int num_inputs;

};

typedef struct Concat* CONCAT;

struct Clip {
	DATA* input;
	DATA* output;
	int size;
	DATA min;
	DATA max;
	
	int qf;
};

typedef struct Clip* CLIP;

struct Pad {
	DATA* input;
	DATA* output;
	
	int size;
	int* sizes;
	int* pad;
	
};

typedef struct Pad* PAD;

struct Crop {
	DATA* input;
	DATA* output;
	
	int size;
	int* sizes;
	int* crop;
	
};

typedef struct Crop* CROP;

struct Floor {
	DATA* input;
	DATA* output;
	int size;
	int qf;
};

typedef struct Floor* FLOOR;


struct Fp2f {
	DATA* input;
	float* output;
	int size;
	int qf;
};

typedef struct Fp2f* FP2F;

struct F2fp {
	float* input;
	DATA* output;
	int size;
	int qf;
};

typedef struct F2fp* F2FP;

struct Flatten {
	DATA* input;
	DATA* output;
};

typedef struct Flatten* FLATTEN;



ADD add_create(void);
MUL mul_create(void);
DIV div_create(void);

ADD_F add_f_create(void);
MUL_F mul_f_create(void);
DIV_F div_f_create(void);

SHIFT shift_create(void);

SIGMOID sigmoid_create(void);

CONCAT concat_create(void);

CLIP clip_create(void);
FLOOR floor_create(void);
PAD pad_create(void);
CROP crop_create(void);


INPUT input_create(void);
INPUT_F input_f_create(void);

FP2F fp2f_create(void);
F2FP f2fp_create(void);

FLATTEN flatten_create(void);

RET add_forward(ADD a);
RET mul_forward(MUL m);
RET div_forward(DIV d);

RET add_f_forward(ADD_F a);
RET mul_f_forward(MUL_F m);
RET div_f_forward(DIV_F d);

RET shift_forward(SHIFT s);
RET sigmoid_forward(SIGMOID s);
RET concat_forward(CONCAT c);

RET clip_forward(CLIP c);
RET floor_forward(FLOOR f);
RET pad_forward(PAD p);
RET crop_forward(CROP c);

RET fp2f_forward(FP2F f);
RET f2fp_forward(F2FP f);

#endif
