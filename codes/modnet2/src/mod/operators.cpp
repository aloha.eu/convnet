#include "operators.h"

#include "paramIO.h"


ADD add_create(void) {
    ADD a = (ADD) calloc(1, sizeof(struct Add));
    return a;
}

MUL mul_create(void) {
    MUL m = (MUL) calloc(1, sizeof(struct Mul));
    return m;
}

DIV div_create(void) {
    DIV d = (DIV) calloc(1, sizeof(struct Div));
    return d;
}

ADD_F add_f_create(void) {
    ADD_F a = (ADD_F) calloc(1, sizeof(struct Add_f));
    return a;
}

INPUT input_create(void) {
    INPUT i = (INPUT) calloc(1, sizeof(struct Input));
    return i;
}

MUL_F mul_f_create(void) {
    MUL_F m = (MUL_F) calloc(1, sizeof(struct Mul_f));
    return m;
}

DIV_F div_f_create(void) {
    DIV_F d = (DIV_F) calloc(1, sizeof(struct Div_f));
    return d;
}

INPUT_F input_f_create(void) {
    INPUT_F i = (INPUT_F) calloc(1, sizeof(struct Input_f));
    return i;
}

SIGMOID sigmoid_create(void) {
    SIGMOID s = (SIGMOID) calloc(1, sizeof(struct Sigmoid));
    return s;
}

CONCAT concat_create(void) {
    CONCAT c = (CONCAT) calloc(1, sizeof(struct Concat));
    return c;
}

SHIFT shift_create(void) {
    SHIFT s = (SHIFT) calloc(1, sizeof(struct Shift));
    return s;
}

CLIP clip_create(void) {
    CLIP c = (CLIP) calloc(1, sizeof(struct Clip));
    return c;
}

PAD pad_create(void) {
    PAD p = (PAD) calloc(1, sizeof(struct Pad));
    return p;
}

CROP crop_create(void) {
    CROP c = (CROP) calloc(1, sizeof(struct Crop));
    return c;
}

FLOOR floor_create(void) {
    FLOOR f = (FLOOR) calloc(1, sizeof(struct Floor));
    return f;
}

FP2F fp2f_create(void) {
    FP2F f = (FP2F) calloc(1, sizeof(struct Fp2f));
    return f;
}

F2FP f2fp_create(void) {
    F2FP f = (F2FP) calloc(1, sizeof(struct F2fp));
    return f;
}


FLATTEN flatten_create(void) {
    FLATTEN f = (FLATTEN) calloc(1, sizeof(struct Flatten));
    return f;
}


RET add_forward(ADD a) {

  DATA* input  = a->input;
  DATA* output = a->output;
  DATA* bias   = a->bias;
  
  int i,j;
  int px = a->size/a->num;
  for (j=0; j<a->num;j++){
    for (i=0; i<px;i++){
      output[j*px + i] = input[j*px + i] + bias[j];
    }
  }
  
  return __OK__;
}


RET add_f_forward(ADD_F a) {

  float* input  = a->input;
  float* output = a->output;
  float* bias   = a->bias;
  
  int i,j;
  int px = a->size/a->num;
  for (j=0; j<a->num;j++){
    for (i=0; i<px;i++){
      output[j*px + i] = input[j*px + i] + bias[j];
    }
  }
  
  return __OK__;
}


RET mul_forward(MUL m) {
 
  DATA* input  = m->input;
  DATA* output = m->output;
  DATA* scaler = m->scaler;
  

  int i,j;
  int px = m->size/m->num;
  for (j=0; j<m->num;j++){
    for (i=0; i<px;i++){
      output[j*px + i] = (input[j*px + i] * scaler[j])>>m->qf;
    }
  }
  
  return __OK__;
}


RET mul_f_forward(MUL_F m) {
 
  float* input  = m->input;
  float* output = m->output;
  float* scaler = m->scaler;
  

  int i,j;
  int px = m->size/m->num;
  for (j=0; j<m->num;j++){
    for (i=0; i<px;i++){
      output[j*px + i] = (input[j*px + i] * scaler[j]);
    }
  }
  
  return __OK__;
}


RET div_forward(DIV d) {
 
  DATA* input   = d->input;
  DATA* output  = d->output;
  DATA* divider = d->divider;

  int i,j;
  int px = d->size/d->num;
  for (j=0; j<d->num;j++){
    for (i=0; i<px;i++){      
      output[j*px + i]=((input[j*px + i]<<d->qf)/ (divider[j]<<d->qf));
    }
  }
  
  return __OK__;
}


RET div_f_forward(DIV_F d) {
 
  float* input   = d->input;
  float* output  = d->output;
  float* divider = d->divider;

  int i,j;
  int px = d->size/d->num;
  for (j=0; j<d->num;j++){
    for (i=0; i<px;i++){      
      output[j*px + i] = input[j*px + i] / divider[j];
    }
  }
  
  return __OK__;
}

RET shift_forward(SHIFT s) {
  int i;
  for (i=0; i<s->size;i++){
    if (s->dir==0)
      s->output[i] = s->input[i] >> s->bit;
    else
      s->output[i] = s->input[i] << s->bit;
      
  }
  return __OK__;
}

RET sigmoid_forward(SIGMOID s) {
  
  // y = 1 / (1 + exp(-x))
  
  int i;
  float t;
  for (i=0; i<s->size;i++){
    t = FIXED2FLOAT(s->input[i], s->qf);
    t = 1/ (1+exp(-t));
    s->output[i]=FLOAT2FIXED(t,s->qf);
  }
}


RET concat_forward(CONCAT c) {  


  _dprintf_("   concatenation of %d inputs\n", c->num_inputs);


  int i;
  int offset=0;
  for (i=0; i<c->num_inputs;++i){
    _dprintf_("\tinput Checksum: %lld\n", checksum(c->input[i], c->size[i]));
    memcpy(c->output + offset, c->input[i], c->size[i] * sizeof(DATA));
    offset+=c->size[i];
  }
  
    _dprintf_("\tOutput Checksum: %lld\n", checksum(c->output, offset));
    
  
}

RET clip_forward(CLIP c) {
  int i;
  for (i=0; i<c->size;i++){
    if (c->input[i]<c->min)
      c->output[i]=c->min;
      
    else if (c->input[i]>c->max)
      c->output[i]=c->max;
    else
      c->output[i]=c->input[i];
  }
  return __OK__;
}

RET _pad_forward(PAD p) {
  int i, j, k, l;
  int osizes[4]={0,0,0,0};
  
  osizes[0]=p->sizes[0]+p->pad[0]+p->pad[0+4];
  osizes[1]=p->sizes[1]+p->pad[1]+p->pad[1+4];
  osizes[2]=p->sizes[2]+p->pad[2]+p->pad[2+4];
  osizes[3]=p->sizes[3]+p->pad[3]+p->pad[3+4];
  
  int i_index=0;
  int o_index=0;
  for (i=0; i<osizes[0];i++){
    for (j=0; j<osizes[1];j++){
      for (k=0; k<osizes[2];k++){
        for (l=0; l<osizes[3];l++){
          if (i<p->pad[0] || i>p->sizes[0]+p->pad[0] ||
              j<p->pad[1] || j>p->sizes[1]+p->pad[1] ||
              k<p->pad[2] || k>p->sizes[2]+p->pad[2] ||
              l<p->pad[3] || l>p->sizes[3]+p->pad[3]) {
              
            p->output[o_index]=0;
            
          }
          else{
            p->output[o_index]=p->input[i_index];
            ++i_index;
          }
          ++o_index;
        }    
  
      }    
  
    }    
  
  }    
  return __OK__;
}

RET pad_forward(PAD p) {
  int i, j, k, l;
  int osizes[3]={0,0,0};
  
  osizes[0]=p->sizes[0];
  osizes[1]=p->sizes[1]+p->pad[0]+p->pad[1];
  osizes[2]=p->sizes[2]+p->pad[2]+p->pad[3];
  
  int a=0;
  int i_index=0;
  int o_index=0;
  for (i=0; i<osizes[0];i++){
    for (j=0; j<osizes[1];j++){
      for (k=0; k<osizes[2];k++){
        if (j<p->pad[0] || j>=p->sizes[1]+p->pad[0] ||
            k<p->pad[2] || k>=p->sizes[2]+p->pad[2]) {
            
          p->output[o_index]=0;
          a++;
        }
        else{
          p->output[o_index]=p->input[i_index];
          ++i_index;
        }
        ++o_index;
  
      }
  
    }
  
  }
  return __OK__;
}

RET crop_forward(CROP c) {
  int i, j, k, l;
  int osizes[3]={0,0,0};
  
  osizes[0]=c->sizes[0];
  osizes[1]=c->sizes[1]-c->crop[0]-c->crop[1];
  osizes[2]=c->sizes[2]-c->crop[2]-c->crop[3];

  int i_index=0;
  int o_index=0;
  for (i=0; i<c->sizes[0];i++){
    for (j=0; j<c->sizes[1];j++){
      for (k=0; k<c->sizes[2];k++){
        if (j>=c->crop[0] && j<osizes[1]+c->crop[0] &&
            k>=c->crop[2] && k<osizes[2]+c->crop[2]) {
            
          c->output[o_index]=c->input[i_index];
          ++o_index;
          
        }
        ++i_index;
  
      }
  
    }
  
  }
  
  
  return __OK__;
}


RET floor_forward(FLOOR f) {
  
  for (int i=0; i<f->size;i++){
    int j = (f->input[i]>>f->qf)<<f->qf; //integer part
    int d = f->input[i]-j;         // decimal part
    
    if (d>0){ // if the decimal part is not 0
      if (f->input[i]>0)
        f->output[i]=j+1<<f->qf;
      else
        f->output[i]=j;
    }
  }
  return __OK__;
}



RET fp2f_forward(FP2F f) {
  
  _dprintf_("   convert fixed point to float. size: %d\n", f->size);
  _dprintf_("\tinput Checksum: %lld\n", checksum(f->input, f->size));
  for (int i=0; i<f->size;i++){
    
    f->output[i] = FIXED2FLOAT(f->input[i],f->qf);
    
  }
  
  _dprintf_("\tOutput Checksum: %.4f\n", checksum_f32(f->output, f->size));
    
  return __OK__;
}


RET f2fp_forward(F2FP f) {
  
  for (int i=0; i<f->size;i++){
    
    f->output[i] = FLOAT2FIXED(f->input[i],f->qf);
    
  }
  return __OK__;
}


