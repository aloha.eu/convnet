#ifndef BATCH_H
#define BATCH_H

#include "types.h"
#include "spatconv.h"

typedef struct Batch* BATCH;

BATCH batch_create(void);

RET batch_forward(BATCH bat, DATA* input, DATA* output, SIZE size[3]);
RET batch_forward_wrap(BATCH bc);

RET batch_init(BATCH bc, NAME weightsFile, NAME biasFile, NAME meanFile,
		NAME varFile, SIZE planes);

RET batch_destroy(BATCH bat);

struct Batch {
    DATA* input;
    DATA* output;
    
	DATA* weights;
	DATA* bias;
	DATA* mean;
	DATA* var;
	
    VARSIZE size[3];
	int qf;
};




typedef struct Batch_f* BATCH_F;

BATCH_F batch_f_create(void);

RET batch_f_forward(BATCH_F bat, float* input, float* output, SIZE size[3]);
RET batch_f_forward_wrap(BATCH_F bc);

RET batch_f_init(BATCH_F bc, NAME weightsFile, NAME biasFile, NAME meanFile,
		NAME varFile, SIZE planes);

RET batch_f_destroy(BATCH_F bat);

struct Batch_f {
    float* input;
    float* output;
    
	float* weights;
	float* bias;
	float* mean;
	float* var;
	
    VARSIZE size[3];
	int qf;
};

#endif
