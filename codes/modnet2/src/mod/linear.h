#ifndef LINEAR_H
#define LINEAR_H

#include "types.h"

struct Linear {
	DATA* weights;
	DATA* bias;
	VARSIZE in_s;
	VARSIZE out_s;
	int qf;
	DATA* input;
	DATA* output;
    
	
};

typedef struct Linear* LINEAR;


struct Linear_f {
	float* weights;
	float* bias;
	VARSIZE in_s;
	VARSIZE out_s;
	int qf;
	float* input;
	float* output;
    
	
};

typedef struct Linear_f* LINEAR_F;


LINEAR linear_create(void);

RET linear_forward_wrap(LINEAR lin);

RET linear_init(LINEAR lin, NAME weightsFile, NAME biasFile, SIZE in_s,
		SIZE out_s);

RET linear_forward(LINEAR lin, DATA* input, DATA* output, SIZE in_s,
		SIZE out_s);

RET linear_destroy(LINEAR lin);


LINEAR_F linear_f_create(void);

RET linear_f_forward_wrap(LINEAR_F lin);

RET linear_f_init(LINEAR_F lin, NAME weightsFile, NAME biasFile, SIZE in_s,
		SIZE out_s);

RET linear_f_forward(LINEAR_F lin, float* input, float* output, SIZE in_s,
		SIZE out_s);

RET linear_f_destroy(LINEAR_F lin);

#endif
