#include "scalar.h"



ADD add_create(void) {
    ADD a = (ADD) calloc(1, sizeof(struct Add));
    return a;
}

MUL mul_create(void) {
    MUL m = (MUL) calloc(1, sizeof(struct Mul));
    return m;
}

SIGMOID sigmoid_create(void) {
    SIGMOID s = (SIGMOID) calloc(1, sizeof(struct Sigmoid));
    return s;
}




RET add_forward(ADD a) {
  int i;
  for (i=0; i<a->size;i++)
    a->output[i]=a->input[i]+a->bias;
    return __OK__;
}

RET mul_forward(MUL m) {
  int i;
  for (i=0; i<m->size;i++){
#ifdef _FIXED_
    m->output[i]=(m->input[i]*m->scaler)>>m->qf;

}
#else
    m->output[i]=m->input[i]*m->scaler;
}
#endif
   return __OK__; 
}

RET sigmoid_forward(SIGMOID s) {
  
  // y = 1 / (1 + exp(-x))
  
  int k;
  float t;
  for (k=0; k<s->size;k++){
    t = FIXED2FLOAT(s->input[k], s->qf);
    t = 1/ (1+exp(-t));
    s->output[k]=FLOAT2FIXED(t,s->qf);
  }
  return __OK__;
}
