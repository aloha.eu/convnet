#include "maxpool.h"
#include "xassert.h"
#include "paramIO.h"



MAXPOOL maxpool_create(void) {
    MAXPOOL mp = (MAXPOOL) calloc(1, sizeof(struct Maxpool));
    return mp;
}



RET maxpool_forward_sw_core(DATA* input, DATA* output, SIZE in_s[3], SIZE out_s[3], SIZE kern_s[2], SIZE stride[2], SIZE pad[4],int mode, int qf);


RET maxpool_forward_wrap (MAXPOOL mp){

return maxpool_forward(mp->input, mp->output, mp->in_s, mp->out_s, mp->kern_s, mp->stride, mp->pad, mp->mode, mp->qf);

}



RET maxpool_forward(DATA* input, DATA* output, SIZE in_s[3], SIZE out_s[3],
		SIZE kern_s[2], SIZE stride[2], SIZE pad[4], int mode, int qf) {
	VARSIZE out_a[3] = { 0 };

	out_a[0] = in_s[0];
	out_a[1] = (in_s[1] +  pad[0] + pad[1] - kern_s[0]) / stride[0] + 1;
	out_a[2] = (in_s[2] +  pad[2] + pad[3] - kern_s[1]) / stride[1] + 1;
  
	ASSERT(equalSize(out_s, out_a, 3), "%s",
			"output size does not match parameterized pooling kernel");
			
  
  _dprintf_("[%dx%d] software pooling. mode: %d:\n", kern_s[2], kern_s[3], mode);
  _dprintf_("\t%lu to %lu\n", in_s[0], out_s[0]);
  _dprintf_("\t%lux%lu to %lux%lu\n", in_s[1], in_s[2], out_s[1], out_s[2]);

  VARSIZE initial_pad[2];
  initial_pad[0] = pad[0];
  initial_pad[1] = pad[2];
	_tcreate_(time);

	maxpool_forward_sw_core(input, output, in_s, out_s, kern_s, stride, initial_pad, mode, qf);
	_tprintf_("MaxPool time: %5.3f ms\n", (get_wall_time()-time)/1000);

    _dprintf_("\tInput Checksum: %lld\n", checksum(input, in_s[0]*in_s[1]*in_s[2]));
    _dprintf_("\tInput Min: %d Max: %d Avg: %d\n", min(input, in_s[0]*in_s[1]*in_s[2]), max(input, in_s[0]*in_s[1]*in_s[2]), avg(input, in_s[0]*in_s[1]*in_s[2]));
    
    _dprintf_("\tOutput Checksum: %lld\n", checksum(output, out_s[0]*out_s[1]*out_s[2]));
    
	return __OK__;
}

RET maxpool_forward_sw_core(DATA* input, DATA* output, SIZE in_s[3],
		SIZE out_s[3], SIZE kern_s[2], SIZE stride[2], SIZE pad[2],int mode, int qf) {

	ITER plane = 0;
	ITER hout = 0;
	ITER wout = 0;
	ITER hkern = 0;
	ITER wkern = 0;
	ITER _stride0 = stride[0];
	ITER _stride1 = stride[1];
	ITER _of   = out_s[0];
	ITER _of_h = out_s[1];
	ITER _of_w = out_s[2];
	ITER _if   = in_s[0];
	ITER _if_h = in_s[1];
	ITER _if_w = in_s[2];
	ITER _k_h = kern_s[0];
	ITER _k_w = kern_s[1];
	ITER _pad_h = pad[0];
	ITER _pad_w = pad[1];
	
	int hin = 0;
	int win = 0;
	int cond = 0;

	DATA current = 0;
	DATA out = 0;

	/* for each plane */
	#pragma omp parallel for firstprivate(_stride0, _stride1, _of, _of_h, _of_w, _if, _if_h, _if_w, _k_h, _k_w, _pad_h, _pad_w) private(out, current, hout,wout,hkern,wkern,hin,win,cond) collapse(2)
	for (plane = 0; plane < _of; plane++) {
		/* for output matrix */
		for (hout = 0; hout < _of_h; hout++) {
			ITER of_h_idx = (plane*_of_h + hout)*_of_w;

			for (wout = 0; wout < _of_w; wout++) {
				bool first_element = true;

				/* for kernel matrix */
				for (hkern = 0; hkern < _k_h; hkern++) {
					/* calculate required input position */
					hin = _stride0 * hout + hkern - _pad_h;
					ITER if_h_idx = (plane*_if_h + hin)*_if_w;

					for (wkern = 0; wkern < _k_w; wkern++) {
						/* calculate required input position */
						win = _stride1 * wout + wkern - _pad_w;

						/* test if position is inside bounds*/
						cond = hin >=0 && win >= 0 && (ITER) win < _if_w && (ITER) hin < _if_h;

						/* if outside bounds => set to zero */
						DATA inVal;
						if (cond) {
							//inVal = in[plane][hin][win];
							inVal = input[if_h_idx + win];
						} else {
							inVal = 0;
						}

						current = inVal;

						switch (mode){
						case 0: //maxpool
						  if(first_element){
							out = current;
							first_element = false;
						  } else {
							out = (out > current) ? out : current;
						  }
						  break;
						case 1: // subsampling
						  if(first_element){
							out = current;
							first_element = false;
						  }
						  break;
						case 2: //avgpool
						  if(first_element){
							out = current;
							first_element = false;
						  } else {
							out += current;
						  }
						  
						  break;
						default:
						  printf ("Error in pooling mode!\n");
						  exit(1);
						  break;
						}
						
					}
				}
				switch (mode){
				case 0: //maxpool
				  out = out;
				  break;
				case 1: // subsampling
				  out = out;
				  break;
				case 2: //avgpool
				  {
          int pooling_s = (kern_s[0] * kern_s[1]) << qf;
          out = (int)out<<qf / pooling_s;
				  }
				  break;
				default:
				  printf ("Error in pooling mode!\n");
				  exit (1);
				  break;
				}
				output[of_h_idx + wout] = out;
			}
		}
	}
	return __OK__;
}
