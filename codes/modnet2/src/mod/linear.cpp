#include "linear.h"
#include "spatconv.h"
#include "xassert.h"
#include "paramIO.h"

RET linear_forward_sw_core(DATA* input, DATA* output, SIZE in_s, SIZE out_s,
		DATA* weights, DATA* bias, int qf);

RET linear_f_forward_sw_core(float* input, float* output, SIZE in_s, SIZE out_s,
		float* weights, float* bias, int qf);
		
LINEAR linear_create(void) {
	LINEAR lin = (LINEAR) calloc(1, sizeof(struct Linear));
	return lin;
}

LINEAR_F linear_f_create(void) {
	LINEAR_F lin = (LINEAR_F) calloc(1, sizeof(struct Linear_f));
	return lin;
}

RET linear_init(LINEAR lin, NAME weightsFile, NAME biasFile, SIZE in_s,
		SIZE out_s) {
	SIZE weight_s = out_s * in_s;
	SIZE bias_s = out_s;
	RET res = __OK__;

	lin->weights = (DATA*) calloc(weight_s, sizeof(DATA));
	lin->bias = (DATA*) calloc(bias_s, sizeof(DATA));

	res = loadData(weightsFile, weight_s, lin->weights);
	ASSERT(res == __OK__, "%s", "loading kernel tensor failed");

	res = loadData(biasFile, bias_s, lin->bias);
	ASSERT(res == __OK__, "%s", "loading bias tensor failed");

	lin->in_s = in_s;
	lin->out_s = out_s;
	return __OK__;
}

RET linear_f_init(LINEAR_F lin, NAME weightsFile, NAME biasFile, SIZE in_s,
		SIZE out_s) {
	SIZE weight_s = out_s * in_s;
	SIZE bias_s = out_s;
	RET res = __OK__;

	lin->weights = (float*) calloc(weight_s, sizeof(float));
	lin->bias = (float*) calloc(bias_s, sizeof(float));

	res = load_float(weightsFile, weight_s, lin->weights);
	ASSERT(res == __OK__, "%s", "loading kernel tensor failed");

	res = load_float(biasFile, bias_s, lin->bias);
	ASSERT(res == __OK__, "%s", "loading bias tensor failed");

	lin->in_s = in_s;
	lin->out_s = out_s;
	return __OK__;
}


RET linear_forward_wrap(LINEAR lin){

return linear_forward(lin, lin->input, lin->output, lin->in_s, lin->out_s);

}


RET linear_f_forward_wrap(LINEAR_F lin){

return linear_f_forward(lin, lin->input, lin->output, lin->in_s, lin->out_s);

}


RET linear_forward(LINEAR lin, DATA* input, DATA* output, SIZE in_s,
		SIZE out_s) {

	ASSERT(in_s == lin->in_s, "%s", "input size does not match");
	ASSERT(out_s == lin->out_s, "%s", "output size does not match");

    _dprintf_("      software linear:\n");
    _dprintf_("\t%lu to %lu\n", in_s, out_s);
    
	_tcreate_(time);
	linear_forward_sw_core(input, output, in_s, out_s, lin->weights,
			lin->bias, lin->qf);
	_tprintf_("Fully-Connected time: %5.3f ms\n", (get_wall_time()-time)/1000);

    _dprintf_("\tInput Checksum: %lld\n", checksum(input, in_s));
    _dprintf_("\tInput Min: %d Max: %d Avg: %d\n", min(input, in_s), max(input, in_s), avg(input, in_s));
    
    _dprintf_("\tOutput Checksum: %lld\n", checksum(output, out_s));
    _dprintf_("\tWeights Checksum: %lld\n", checksum(lin->weights, in_s*out_s));
    
	return __OK__;
}

RET linear_f_forward(LINEAR_F lin, float* input, float* output, SIZE in_s, SIZE out_s) {

	ASSERT(in_s == lin->in_s, "%s", "input size does not match");
	ASSERT(out_s == lin->out_s, "%s", "output size does not match");

    _dprintf_("      software linear float:\n");
    _dprintf_("\t%lu to %lu\n", in_s, out_s);
    
	_tcreate_(time);
	linear_f_forward_sw_core(input, output, in_s, out_s, lin->weights, lin->bias, lin->qf);
	_tprintf_("Fully-Connected time: %5.3f ms\n", (get_wall_time()-time)/1000);

    _dprintf_("\tInput Checksum: %.4f\n", checksum_f32(input, in_s));
    _dprintf_("\tInput Min: %.4f Max: %.4f Avg: %.4f\n", min_f32(input, in_s), max_f32(input, in_s), avg_f32(input, in_s));
    
    _dprintf_("\tOutput Checksum: %.4f\n", checksum_f32(output, out_s));
    _dprintf_("\tWeights Checksum: %.4f\n", checksum_f32(lin->weights, in_s*out_s));
    
	return __OK__;
}

RET linear_forward_sw_core(DATA* input, DATA* output, SIZE in_s, SIZE out_s,
		DATA* weights, DATA* bias, int qf) {

	// NOTE return W * x

	ITER hkern = 0;
	ITER wkern = 0;

#ifdef _FIXED_
	long long int mac = 0;
#else
	DATA mac = 0;
#endif

	DATA current = 0;

	/* foreach row in kernel */
	#pragma omp parallel for private (hkern, wkern, mac, current)
	for (hkern = 0; hkern < out_s; hkern++) {

	#ifdef _FIXED_
		mac = ((long long int)bias[hkern]) << qf;
	#else
		mac = bias[hkern]; 
	#endif
		
		for (wkern = 0; wkern < in_s; wkern++) {
			current = input[wkern];
			mac += current * weights[hkern*in_s + wkern];
		}



		#ifdef _FIXED_
			output[hkern] = (DATA)saturate(mac >> qf, "linear");
		#else
			output[hkern] = mac;
		#endif

	}

	return __OK__;
}

RET linear_f_forward_sw_core(float* input, float* output, SIZE in_s, SIZE out_s,
		float* weights, float* bias, int qf) {

	// NOTE return W * x

	ITER hkern = 0;
	ITER wkern = 0;

	float mac = 0;


	float current = 0;

	/* foreach row in kernel */
	#pragma omp parallel for private (hkern, wkern, mac, current)
	for (hkern = 0; hkern < out_s; hkern++) {

		mac = bias[hkern]; 
	
		
		for (wkern = 0; wkern < in_s; wkern++) {
			current = input[wkern];
			mac += current * weights[hkern*in_s + wkern];
		}


		output[hkern] = mac;
		

	}

	return __OK__;
}

RET linear_destroy(LINEAR lin) {
	free(lin->weights);
	free(lin->bias);
	free(lin);
	return __OK__;
}

RET linear_f_destroy(LINEAR_F lin) {
	free(lin->weights);
	free(lin->bias);
	free(lin);
	return __OK__;
}
