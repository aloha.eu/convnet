#ifndef MAXPOOL_H
#define MAXPOOL_H

#include "types.h"

struct Maxpool {
	DATA* input;
	DATA* output;
	
	VARSIZE in_s[3];
	VARSIZE out_s[3];
	
	VARSIZE kern_s[2];
	VARSIZE stride[2];
	VARSIZE pad[4];
	int mode;
	int qf;
};

typedef struct Maxpool* MAXPOOL;

RET maxpool_forward(DATA* input, DATA* output, SIZE in_s[3], SIZE out_s[3],
		SIZE kern_s[2], SIZE stride[2], SIZE pad[4],int mode, int qf);
MAXPOOL maxpool_create(void);

RET maxpool_forward_wrap(MAXPOOL mp);
#endif
