#include "region.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include "paramIO.h"

//char *names[] = { "person", "car", "dog", "cat", "bottle", "bus", "car", "cat", "chair", "cow", "diningtable", "dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train", "tvmonitor"};



REGION region_create(void) {
    REGION r = (REGION) calloc(1, sizeof(struct Region));
    return r;
}

RET region_forward_wrap(REGION r) {

  int l_output =r->h_grid * r->w_grid * r->num_boxes * (r->num_classes+4+1);
  
  return region_forward(r->input, l_output, r->num_boxes, r->h_grid, r->w_grid, r->num_classes, r->anchors, r->confidence_trsh, r->relative, r->overlap_trsh, r->classes);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//FORWARD REGION LAYER
//activation and softmax

void activate_array(float *x, const int n)
{
    int i;
    for(i = 0; i < n; ++i){
      x[i] = 1./(1. + exp(-x[i]));
    }
}


void softmax(float *input, int n, float temp, int stride, float *output)
{
    int i;
    float sum = 0;
    float largest = -FLT_MAX;
    for(i = 0; i < n; ++i){
        if(input[i*stride] > largest) largest = input[i*stride];
    }
    for(i = 0; i < n; ++i){
        float e = exp(input[i*stride]/temp - largest/temp);
        sum += e;
        output[i*stride] = e;
    }
    for(i = 0; i < n; ++i){
        output[i*stride] /= sum;
    }
}


void softmax_cpu(float *input, int n, int batch, int batch_offset, int groups, int group_offset, int stride, float temp, float *output)
{
    int g, b;
    for(b = 0; b < batch; ++b){
        for(g = 0; g < groups; ++g){
            softmax(input + b*batch_offset + g*group_offset, n, temp, stride, output + b*batch_offset + g*group_offset);
        }
    }
}



int entry_index(int w, int h, int coords, int classes, int outputs , int batch, int location, int entry)
{
    int n =   location / (w*h); 	//num of boxes
    int loc = location % (w*h);
    return batch*outputs + n*w*h*(coords+classes+1) + entry*w*h + loc;
}


void forward_region_layer(float* output, int l_batch, int l_outputs, int l_n, int l_h, int l_w, int l_coords, int l_classes)
{
    int i,j,b,t,n;
    for (b = 0; b < l_batch; ++b){
        for(n = 0; n < l_n; ++n){
            int index = entry_index(l_w,l_h,l_coords,l_classes,l_outputs, b, n*l_w*l_h, 0);
            activate_array(output + index, 2*l_w*l_h);
            index = entry_index(l_w,l_h,l_coords,l_classes,l_outputs, b, n*l_w*l_h, l_coords);
            activate_array(output + index,   l_w*l_h);
//            index = entry_index(l_w,l_h,l_coords,l_classes,l_outputs, b, n*l_w*l_h, l_coords + 1);
        }
    }
    int index = entry_index(l_w,l_h,l_coords,l_classes,l_outputs, 0, 0, l_coords + 1);
    softmax_cpu(output + index, l_classes + 0, l_batch*l_n, l_outputs/l_n, l_w*l_h, 1, l_w*l_h, 1, output + index);
    return;    
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//BOXES
//check for overlapping boxes 


int nms_comparator(const void *pa, const void *pb)
{
    detection a = *(detection *)pa;
    detection b = *(detection *)pb;
    float diff = 0;
    if(b.sort_class >= 0){
        diff = a.prob[b.sort_class] - b.prob[b.sort_class];
    } else {
        diff = a.objectness - b.objectness;
    }
    if(diff < 0) return 1;
    else if(diff > 0) return -1;
    return 0;
}

float overlap(float x1, float w1, float x2, float w2)
{
    float l1 = x1 - w1/2;
    float l2 = x2 - w2/2;
    float left = l1 > l2 ? l1 : l2;
    float r1 = x1 + w1/2;
    float r2 = x2 + w2/2;
    float right = r1 < r2 ? r1 : r2;
    return right - left;
}

float box_intersection(box a, box b)
{
    float w = overlap(a.x, a.w, b.x, b.w);
    float h = overlap(a.y, a.h, b.y, b.h);
    if(w < 0 || h < 0) return 0;
    float area = w*h;
    return area;
}

float box_union(box a, box b)
{
    float i = box_intersection(a, b);
    float u = a.w*a.h + b.w*b.h - i;
    
    //printf ("0001\n",u);
    
    return u;
}

float box_iou(box a, box b)
{
    return box_intersection(a, b)/box_union(a, b);
}

void do_nms_sort(detection *dets, int total, int classes, float nms_thresh)
{

    //printf("a1\n");

    int i, j, k;
    k = total-1;
    for(i = 0; i <= k; ++i){
        if(dets[i].objectness == 0){
            detection swap = dets[i];
            dets[i] = dets[k];
            dets[k] = swap;
            --k;
            --i;
        }
    }
    total = k+1;
    //printf("a2\n");

    for(k = 0; k < classes; ++k){
        for(i = 0; i < total; ++i){
            dets[i].sort_class = k;
        }
        qsort(dets, total, sizeof(detection), nms_comparator);		//orders dets array based on prob or objectness, then compares each box with the following ones evaluating iou
        for(i = 0; i < total; ++i){
            if(dets[i].prob[k] == 0) continue;
            box a = dets[i].bbox;
            for(j = i+1; j < total; ++j){
                box b = dets[j].bbox;
               // printf ("0001\n");
                if (box_iou(a, b) > nms_thresh){
                    dets[j].prob[k] = 0;
                }
               // printf ("0002\n");
            }
        }
    }
    
  //  printf("a3\n");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//BOXES
//region detections


box get_region_box(float *x, float *anchors, int n, int index, int i, int j, int w, int h, int stride)
{
    box b;
    b.x = (i + x[index + 0*stride]) / w;
    b.y = (j + x[index + 1*stride]) / h;
    b.w = exp(x[index + 2*stride]) * anchors[2*n]   / w;
    b.h = exp(x[index + 3*stride]) * anchors[2*n+1] / h;
    return b;
}

void correct_region_boxes(detection *dets, int n, int w, int h, int netw, int neth, int relative)	//produces no effects on square grids and square images
{
    int i;
    int new_w=0;
    int new_h=0;
    if (((float)netw/w) < ((float)neth/h)) {
        new_w = netw;
        new_h = (h * netw)/w;
    } else {
        new_h = neth;
        new_w = (w * neth)/h;
    }
    for (i = 0; i < n; ++i){
        box b = dets[i].bbox;
        b.x =  (b.x - (netw - new_w)/2./netw) / ((float)new_w/netw); 
        b.y =  (b.y - (neth - new_h)/2./neth) / ((float)new_h/neth); 
        b.w *= (float)netw/new_w;
        b.h *= (float)neth/new_h;
        if(!relative){
            b.x *= w;
            b.w *= w;
            b.y *= h;
            b.h *= h;
        }
        dets[i].bbox = b;
    }
}

void get_region_detections(int l_h, int l_w, int l_n, int l_classes, int l_coords, float* anchors, float *output, float conf_thresh, int relative, detection *dets)
{

    //printf("l_h %d\n",l_h);
    //printf("l_w %d\n",l_w);
    //printf("l_n %d\n",l_n);
    //printf("l_classes %d\n",l_classes);
    //printf("l_coords %d\n",l_coords);

    int i,j,n,z;
    float *predictions = output; 	//output of last convolution

    for (i = 0; i < l_w*l_h; ++i){ 	//inside grid cell
        int row = i / l_w;
        int col = i % l_w;
        for(n = 0; n < l_n; ++n){	//inside box
            int index = n*l_w*l_h + i;
            for(j = 0; j < l_classes; ++j){
                dets[index].prob[j] = 0;
            }
            int obj_index  = entry_index(l_w, l_h, l_coords, l_classes, n*l_w*l_h*(l_classes+l_coords+1), 0, n*l_w*l_h + i, l_coords);
            int box_index  = entry_index(l_w, l_h, l_coords, l_classes, n*l_w*l_h*(l_classes+l_coords+1), 0, n*l_w*l_h + i, 0);
            int mask_index = entry_index(l_w, l_h, l_coords, l_classes, n*l_w*l_h*(l_classes+l_coords+1), 0, n*l_w*l_h + i, 4);

            float scale = predictions[obj_index];
//            dets[index].bbox = get_region_box(predictions, anchors, n, box_index, col, row, l_w, l_h, l_w*l_h);
            dets[index].objectness = scale > conf_thresh ? scale : 0;
            if(dets[index].mask){ 	//skipped when coords=4
                for(j = 0; j < l_coords - 4; ++j){
                    dets[index].mask[j] = output[mask_index + j*l_w*l_h];
                }
            }
            int class_index = entry_index(l_w, l_h, l_coords, l_classes, n*l_w*l_h*(l_classes+l_coords+1), 0, n*l_w*l_h + i, l_coords + 1);
                if(dets[index].objectness){
                    for(j = 0; j < l_classes; ++j){
                        int class_index = entry_index(l_w, l_h, l_coords, l_classes, n*l_w*l_h*(l_classes+l_coords+1), 0, n*l_w*l_h + i, l_coords + 1 + j);
                        float prob = scale*predictions[class_index];
                        dets[index].prob[j] = (prob > conf_thresh) ? prob : 0;
                        dets[index].bbox = get_region_box(predictions, anchors, n, box_index, col, row, l_w, l_h, l_w*l_h);
                    }
                }
        }
    }
//    correct_region_boxes(dets, l_w*l_h*l_n, w, h, netw, neth, relative);   //magari aggiungere un if con una condizione di esecuzione
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//REGION LAYER

RET region_forward(float *input, int l_outputs, int n_boxes, int l_h, int l_w, int l_classes, float *anchors,
                   float conf_thresh, int relative, float nms_thresh, char** classes){

  _dprintf_("       region layer:\n");
  _dprintf_("\tGrid %dx%d\n", l_h, l_w);
  _dprintf_("\tClasses %d\n", l_classes);
  _dprintf_("\tThresholds: nms %0.2f, confidence %0.2f\n", nms_thresh, conf_thresh);


  _dprintf_("\tInput Checksum: %f\n", checksum_f32(input, l_outputs));
  _dprintf_("\tInput Min: %f Max: %f Avg: %f\n", min_f32(input, l_outputs), max_f32(input, l_outputs), avg_f32(input, l_outputs));

    
  int i;
  int batch_size = 1;
  
  forward_region_layer(input, batch_size, l_outputs, n_boxes, l_h, l_w, 4, l_classes);

  detection* dets;
  dets=(detection*)malloc(n_boxes*l_w*l_h*sizeof(detection));

  for(i = 0; i < n_boxes*l_h*l_w; i++){
          dets[i].prob = (float*)malloc(l_classes*sizeof(float));	
  }


  get_region_detections(l_h, l_w, n_boxes, l_classes, 4, anchors, input, conf_thresh, relative, dets); 


  do_nms_sort(dets, n_boxes*l_w*l_h, l_classes, nms_thresh);

  FILE *fp;
  fp = fopen("results.txt", "a");
          if (fp == NULL){
            printf("Could not open file results.txt");
            exit( 1);
          }
  int nobj =0;
  printf("\n\n################ Detected objects #################\n");
  for(int i = 0; i <  n_boxes*l_w*l_h; i++){

          for(int j = 0; j < l_classes; j++){
              if (dets[i].prob[j] > conf_thresh){
                  printf("%s: %.0f%%\n", classes[j], dets[i].prob[j]*100);
                  printf("\t\tbox %f %f %f %f\n",dets[i].bbox.x,dets[i].bbox.y,dets[i].bbox.w,dets[i].bbox.h);
                  
                  fprintf (fp, "\n%s, %.0f, %f, %f, %f, %f", classes[j], dets[i].prob[j]*100,dets[i].bbox.x,dets[i].bbox.y,dets[i].bbox.w,dets[i].bbox.h);
                  nobj++;
              }
          }
  }
  printf("Found objects: %d \n", nobj);
  printf("###################################################\n\n");
   fprintf (fp, "\n\n");
          fclose(fp);
  for(i = 0; i < n_boxes*l_h*l_w; i++){
          free(dets[i].prob);
  }
  free(dets);

  return __OK__;
}
