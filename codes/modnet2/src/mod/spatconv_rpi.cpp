#pragma GCC diagnostic ignored "-Wformat"

#include "spatconv.h"
#include "spatconv_rpi.h"
//#include "channel.h"
#include "xassert.h"

//#include "spatconv_sw.h"
//#include "maxpool.h"
//#include "avgpool.h"

#include "paramIO.h"

RET spatconv_rpi_sw_16(short int* input, short int* output, short int* kernel,
    short int* bias, SIZE in_s[3], SIZE out_s[3], SIZE kern_s[2], SIZE dil[2], SIZE pad[2], SIZE stride[2],
    DATARPI *scalers_bn, DATARPI *biases_bn, DATARPI num, DATARPI scaler, DATARPI divider, DATARPI clip_max,
    DATARPI clip_min, bool activate, int ncol, int groups, int qf) {


  unsigned int OF = out_s[0];
  unsigned int IF = in_s[0];

#ifdef _FIXED_
  long long int mac;
  #if (_NEURAGHE_)
  unsigned int if_count = 0;

    unsigned int _MIN_IF = 0;

    #ifdef NEU_TCN
    _MIN_IF = ncol;
    #else
    if(kern_s[0] == 5)
        _MIN_IF = ncol;
    if(kern_s[0] == 3 || kern_s[0] == 1)
        _MIN_IF = ncol*3;
    #endif

    if(_MIN_IF!=0){

        if( (IF % _MIN_IF) > 0 )
            IF = (IF/_MIN_IF + 1) * _MIN_IF;
    }
    

  #endif
#else
  DATA mac;
#endif

//printf("qf: %d  ncol: %d OF: %d  IF: %d \n", qf, ncol, OF, IF);
  /*DATA*/long long int bi, current, curr_kern;

	int *out = (int*)malloc(OF*out_s[1]*out_s[2]*sizeof(int));
  unsigned int i,j,k,in;


  // foreach output plane
	#ifndef __CI__
int satcount =0;
int max_satcount =30;
#endif
#if (_NEURAGHE_) && _FIXED_
  #pragma omp parallel for firstprivate(mac,bi,current,curr_kern,if_count)
#else
  #pragma omp parallel for firstprivate(mac,bi,current,curr_kern)
#endif
  
  for (ITER pout = 0; pout < OF; pout++) {

    // buffer bias 
    bi = bias[pout];
//if (bi !=0)
		  //printf ("%d \n", pout);
      
    // for output matrix 
    for (ITER hout = 0; hout < out_s[1]; hout++) {
      for (ITER wout = 0; wout < out_s[2]; wout++) {

        // initialise multiply-accumulate to bias 
      #ifdef _FIXED_
        mac = ((long long int)bi) << qf;
      #else
        mac = bi;
      #endif

        // foreach input plane 
        for (ITER pin = 0; pin < IF; pin++) {

          // for kernel matrix 
          for (ITER hkern = 0; hkern < kern_s[0]; hkern++) {

          #if (_NEURAGHE_) && _FIXED_
            if(pin>=in_s[0] || pout>=out_s[0])
              continue;
          #endif

            // calculate required input position 
            int hin = stride[0] * hout + hkern*dil[0] - pad[0];

            // test if position is inside bounds
            bool cond = hin >= 0 && (ITER) hin < in_s[1];

            // if outside bounds => continue 
            if (cond == 0)
              continue;

            for (ITER wkern = 0; wkern < kern_s[1]; wkern++) {

              // calculate required input position 
              int win = stride[1] * wout + wkern*dil[1] - pad[1];

              // test if position is inside bounds
              cond = win >= 0 && (ITER) win < in_s[2];

              // if outside bounds => continue 
              if (cond == 0)
                continue;

              current = input[(pin*in_s[1] + hin)*in_s[2] + win];

              curr_kern = kernel[((pout*in_s[0] + pin)*kern_s[0] + hkern)*kern_s[1] + wkern]; 
              // printf("place: %d  W: %d  -- v: %d\n", current, curr_kern, current*curr_kern);
              mac += current * curr_kern;
              //if (mac>>qf == 0x00001b50)
		 // printf ("bbb %d %d %d %x %x \n", pout, hout, wout, current, curr_kern);

            }
          }
        //  if (mac>>qf == 0x00001b50)
		 // printf ("aa%d %d %d \n", pout, hout, wout);
     
       #if (_NEURAGHE_) && _FIXED_
          #ifdef NEU_TCN
	         if_count++;
	         if(if_count == _MIN_IF) {
		            mac = sat_16(mac >> qf) << qf;
                if_count = 0;
              }
	        #else
          //Emulate Neuraghe platform
          if(kern_s[0]==3 || kern_s[0]==5 || kern_s[0]==1) {
            if_count++;
            if(((kern_s[0] == 3 || kern_s[0]==1) && if_count == _MIN_IF) || (kern_s[0] == 5 && if_count == _MIN_IF)) {
              //mac = (saturate(mac >> qf, "Conv")) << qf;
		          mac = sat_16(mac >> qf) << qf;
              if_count = 0;
            }
          }
          #endif
       #endif
  
        }
        double macrpi;
        double temp;
        
        macrpi = ((double)mac * scalers_bn[pout] + biases_bn[pout])*scaler/divider;
        temp = macrpi;
        if (clip_max>1000){
        //  printf("%f\t",macrpi);
          macrpi=macrpi;
          }
        if (macrpi < clip_min)
          macrpi = clip_min;
        if (macrpi > clip_max)
          macrpi = clip_max;
        mac = (DATA) macrpi;
       //if (temp !=0 && macrpi ==0)
        // printf ("temp %f , macrpi %f\n",temp, macrpi);
       #if  !(_NEURAGHE_) 
         #ifdef _FIXED_
         mac = mac >> qf;
         if((mac & 0xffff8000) == 0xffff8000 | (mac & 0xffff8000) == 0){
           mac = mac;
         }
         else{
          #ifndef __CI__
          int old_val;
          if (satcount<max_satcount)
            old_val= mac;
          
          #endif 
          if(mac & 0x80000000)
            mac = 0xffff8000;
          else
            mac= 0x00007fff;
          
          #ifndef __CI__
          if (satcount<max_satcount)
            printf("[SPATCONV_RPI] saturation 0x%08x -> 0x%08x\n", old_val, mac);
          satcount ++;
          #endif 
          
          }
          mac= mac<<qf;
          #endif
        #endif

	out[(pout*out_s[1] + hout)*out_s[2] + wout] = mac;
	//	if (mac>>qf == 0x00001b50)
	//	  printf ("%d %d %d \n", pout, hout, wout);
        
        //printf("output[%d] -->  fixed: %d  -->  %3.10f\n", (pout*out_s[1] + hout)*out_s[2] + wout, output[(pout*out_s[1] + hout)*out_s[2] + wout], FIXED2FLOAT(output[(pout*out_s[1] + hout)*out_s[2] + wout]) );
      }
    }
  }

	//shift
	for(i=0; i<OF*out_s[1]*out_s[2]; i++){
	  output[i] = out[i] >> qf;
	  if(activate && output[i] < 0)
		  output[i] = 0;
	}
	#ifndef __CI__
	 if (satcount>0)
	   printf("[SPATCONV_RPI] tot num of saturations: %d\n", satcount);
           
  #endif 

  free (out);
  return __OK__;
}

RET spatconv_rpi_forward_wrap(SPATCONV_RPI sc){

  return spatconv_rpi_forward_sw(sc, sc->input, sc->output, sc->in_s, sc->out_s, sc->stride, sc->pad, 
                         sc->scalers_bn, sc->biases_bn, sc->num, sc->scaler, sc->divider, sc->clip_max, sc->clip_min,
                         sc->activate, _N_COL_, sc->groups, sc->qf, sc->precision8);

}

RET spatconv_rpi_forward_sw(SPATCONV_RPI sc, DATA* input, DATA* output, SIZE in_s[3], SIZE out_s[3], SIZE stride[2], SIZE pad[2],
                         DATARPI *scalers_bn, DATARPI *biases_bn, DATARPI num, DATARPI scaler, DATARPI divider, DATARPI clip_max, DATARPI clip_min,
                         bool activate,int ncol, int groups, int qf, int precision8) {


    ASSERT(in_s[0] == sc->pin, "%s", "input planes do not match");
    ASSERT(out_s[0] == sc->pout, "%s", "output planes do not match");

    SIZE* kern_s = &sc->kern_s[0];
    SIZE* dil    = &sc->dil[0];

    int complexity = out_s[0] * in_s[0] * kern_s[2] * kern_s[3] * out_s[1] * out_s[2];

    _dprintf_("[%dx%d] software spatial convolution RPI:\n", kern_s[2], kern_s[3]);
    _dprintf_("\t%lu to %lu\n", in_s[0], out_s[0]);
    _dprintf_("\t%lux%lu to %lux%lu\n", in_s[1], in_s[2], out_s[1], out_s[2]);


    _tcreate_(time);
    spatconv_rpi_sw_16((short int *)input, (short int *)output, (short int *)sc->kernel, (short int *)sc->bias, in_s, out_s, &kern_s[2], &dil[0], pad, stride,
      scalers_bn, biases_bn, num, scaler, divider, clip_max, clip_min,
      activate, ncol,groups, qf);
    _tprintf_("\tConv_exec 16: %5.3f ms,  time/complexity: %5.6f us,  GMACs: %5.2f\n", (get_wall_time()-time)/1000, (get_wall_time()-time)/complexity, (double)complexity/(get_wall_time()-time)/1000);


    _dprintf_("\tInput Checksum: %lld\n", checksum(input, in_s[0]*in_s[1]*in_s[2]));
    _dprintf_("\tInput Min: %d Max: %d Avg: %d\n", min(input, in_s[0]*in_s[1]*in_s[2]), max(input, in_s[0]*in_s[1]*in_s[2]), avg(input, in_s[0]*in_s[1]*in_s[2]));
    
    _dprintf_("\tOutput Checksum: %lld\n", checksum(output, out_s[0]*out_s[1]*out_s[2]));
    _dprintf_("\tWeights Checksum: %lld\n", checksum(sc->kernel, in_s[0]*out_s[0]*kern_s[2]*kern_s[2]));
    
    return __OK__;
}



RET spatconv_rpi_f_sw_16(short int* input, DATARPI* output, short int* kernel,
    short int* bias, SIZE in_s[3], SIZE out_s[3], SIZE kern_s[2], SIZE dil[2], SIZE pad[2], SIZE stride[2],
    DATARPI *scalers_bn, DATARPI *biases_bn, DATARPI num, DATARPI scaler, DATARPI divider, DATARPI clip_max,
    DATARPI clip_min, bool activate, int ncol, int groups, int qf) {


  unsigned int OF = out_s[0];
  unsigned int IF = in_s[0];

#ifdef _FIXED_
  long long int mac;
  #if (_NEURAGHE_)
  unsigned int if_count = 0;

    unsigned int _MIN_IF = 0;

    #ifdef NEU_TCN
    _MIN_IF = ncol;
    #else
    if(kern_s[0] == 5)
        _MIN_IF = ncol;
    if(kern_s[0] == 3 || kern_s[0] == 1)
        _MIN_IF = ncol*3;
    #endif

    if(_MIN_IF!=0){

        if( (IF % _MIN_IF) > 0 )
            IF = (IF/_MIN_IF + 1) * _MIN_IF;
    }
    

  #endif
#else
  DATA mac;
#endif

//printf("qf: %d  scaler: %f divider: %f  IF: %d \n", qf, scaler, divider, IF);
  /*DATA*/long long int bi, current, curr_kern;

	float *out = (float*)malloc(OF*out_s[1]*out_s[2]*sizeof(float));
  unsigned int i,j,k,in;


  // foreach output plane
	#ifndef __CI__
int satcount =0;
int max_satcount =30;
#endif
#if (_NEURAGHE_) && _FIXED_
  #pragma omp parallel for firstprivate(mac,bi,current,curr_kern,if_count)
#else
  #pragma omp parallel for firstprivate(mac,bi,current,curr_kern)
#endif
  
  for (ITER pout = 0; pout < OF; pout++) {
   // printf("scaler %f adder %f\n",scalers_bn[pout], biases_bn[pout]);
    // buffer bias 
    bi = bias[pout];
//if (bi !=0)
		  //printf ("%d \n", pout);
      
    // for output matrix 
    for (ITER hout = 0; hout < out_s[1]; hout++) {
      for (ITER wout = 0; wout < out_s[2]; wout++) {

        // initialise multiply-accumulate to bias 
      #ifdef _FIXED_
        mac = ((long long int)bi) << qf;
      #else
        mac = bi;
      #endif

        // foreach input plane 
        for (ITER pin = 0; pin < IF; pin++) {

          // for kernel matrix 
          for (ITER hkern = 0; hkern < kern_s[0]; hkern++) {

          #if (_NEURAGHE_) && _FIXED_
            if(pin>=in_s[0] || pout>=out_s[0])
              continue;
          #endif

            // calculate required input position 
            int hin = stride[0] * hout + hkern*dil[0] - pad[0];

            // test if position is inside bounds
            bool cond = hin >= 0 && (ITER) hin < in_s[1];

            // if outside bounds => continue 
            if (cond == 0)
              continue;

            for (ITER wkern = 0; wkern < kern_s[1]; wkern++) {

              // calculate required input position 
              int win = stride[1] * wout + wkern*dil[1] - pad[1];

              // test if position is inside bounds
              cond = win >= 0 && (ITER) win < in_s[2];

              // if outside bounds => continue 
              if (cond == 0)
                continue;

              current = input[(pin*in_s[1] + hin)*in_s[2] + win];

              curr_kern = kernel[((pout*in_s[0] + pin)*kern_s[0] + hkern)*kern_s[1] + wkern]; 
              // printf("place: %d  W: %d  -- v: %d\n", current, curr_kern, current*curr_kern);
              mac += current * curr_kern;
              //if (mac>>qf == 0x00001b50)
		 // printf ("bbb %d %d %d %x %x \n", pout, hout, wout, current, curr_kern);

            }
          }
  
        }
        double macrpi;
        double temp;
         //   printf("mac %d mac %f qf %d\n",mac, FIXED2FLOAT(mac,qf), qf);
        mac=mac>>qf;
        macrpi = (FIXED2FLOAT(mac, qf) * scalers_bn[pout] + biases_bn[pout])*scaler/divider;
        temp = macrpi;
        if (clip_max>1000){
       //   printf("%f\t",macrpi);
          macrpi=macrpi;
          }
        if (macrpi < clip_min)
          macrpi = clip_min;
        if (macrpi > clip_max)
          macrpi = clip_max;
       /*
        mac = (DATA) macrpi;
       //if (temp !=0 && macrpi ==0)
        // printf ("temp %f , macrpi %f\n",temp, macrpi);
       #if  !(_NEURAGHE_) 
         #ifdef _FIXED_
         mac = mac >> qf;
         if((mac & 0xffff8000) == 0xffff8000 | (mac & 0xffff8000) == 0){
           mac = mac;
         }
         else{
          #ifndef __CI__
          int old_val;
          if (satcount<max_satcount)
            old_val= mac;
          
          #endif 
          if(mac & 0x80000000)
            mac = 0xffff8000;
          else
            mac= 0x00007fff;
          
          #ifndef __CI__
          if (satcount<max_satcount)
            printf("[SPATCONV_RPI_F] saturation 0x%08x -> 0x%08x\n", old_val, mac);
          satcount ++;
          #endif 
          
          }
          mac= mac<<qf;
          #endif
        #endif
        */

    out[(pout*out_s[1] + hout)*out_s[2] + wout] = (DATARPI)macrpi;
  }
    }
  }

	//shift
	for(i=0; i<OF*out_s[1]*out_s[2]; i++){
	  output[i] = out[i] ;
	  if(activate && output[i] < 0)
		  output[i] = 0;
	}
	#ifndef __CI__
	 if (satcount>0)
	   printf("[SPATCONV_RPI_F] tot num of saturations: %d\n", satcount);
           
  #endif 

  free (out);
  return __OK__;
}






RET spatconv_rpi_f_forward_wrap(SPATCONV_RPI_F sc){

  return spatconv_rpi_f_forward_sw(sc, sc->input, sc->output, sc->in_s, sc->out_s, sc->stride, sc->pad, 
                         sc->scalers_bn, sc->biases_bn, sc->num, sc->scaler, sc->divider, sc->clip_max, sc->clip_min,
                         sc->activate, _N_COL_, sc->groups, sc->qf, sc->precision8);

}

RET spatconv_rpi_f_forward_sw(SPATCONV_RPI_F sc, DATA* input, DATARPI* output, SIZE in_s[3], SIZE out_s[3], SIZE stride[2], SIZE pad[2],
                         DATARPI *scalers_bn, DATARPI *biases_bn, DATARPI num, DATARPI scaler, DATARPI divider, DATARPI clip_max, DATARPI clip_min,
                         bool activate,int ncol, int groups, int qf, int precision8) {


    ASSERT(in_s[0] == sc->pin, "%s", "input planes do not match");
    ASSERT(out_s[0] == sc->pout, "%s", "output planes do not match");

    SIZE* kern_s = &sc->kern_s[0];
    SIZE* dil    = &sc->dil[0];

    int complexity = out_s[0] * in_s[0] * kern_s[2] * kern_s[3] * out_s[1] * out_s[2];

    _dprintf_("[%dx%d] software spatial convolution RPI:\n", kern_s[2], kern_s[3]);
    _dprintf_("\t%lu to %lu\n", in_s[0], out_s[0]);
    _dprintf_("\t%lux%lu to %lux%lu\n", in_s[1], in_s[2], out_s[1], out_s[2]);


    _tcreate_(time);
    spatconv_rpi_f_sw_16((short int *)input, (DATARPI *)output, (short int *)sc->kernel, (short int *)sc->bias, in_s, out_s, &kern_s[2], &dil[0], pad, stride,
      scalers_bn, biases_bn, num, scaler, divider, clip_max, clip_min, activate, ncol,groups, qf);
    _tprintf_("\tConv_exec 16: %5.3f ms,  time/complexity: %5.6f us,  GMACs: %5.2f\n", (get_wall_time()-time)/1000, (get_wall_time()-time)/complexity, (double)complexity/(get_wall_time()-time)/1000);


    _dprintf_("\tInput Checksum: %lld\n", checksum(input, in_s[0]*in_s[1]*in_s[2]));
    _dprintf_("\tInput Min: %d Max: %d Avg: %d\n", min(input, in_s[0]*in_s[1]*in_s[2]), max(input, in_s[0]*in_s[1]*in_s[2]), avg(input, in_s[0]*in_s[1]*in_s[2]));
    
    _dprintf_("\tOutput Checksum: %f\n", checksum_f32(output, out_s[0]*out_s[1]*out_s[2]));
    _dprintf_("\tWeights Checksum: %lld\n", checksum(sc->kernel, in_s[0]*out_s[0]*kern_s[2]*kern_s[2]));
    
    return __OK__;
}














SPATCONV_RPI spatconv_rpi_create(void) {
    SPATCONV_RPI sc = (SPATCONV_RPI) calloc(1, sizeof(struct Spatconv_rpi));
    return sc;
}




SPATCONV_RPI_F spatconv_rpi_f_create(void) {
    SPATCONV_RPI_F sc = (SPATCONV_RPI_F) calloc(1, sizeof(struct Spatconv_rpi_f));
    return sc;
}



RET spatconv_rpi_destroy(SPATCONV_RPI sc) {

#ifdef _NEURAGHE_
    if(sc->kern_s[2] != 5 && sc->kern_s[2] != 3)
    {       
#endif

    free(sc->kernel);

#ifdef _NEURAGHE_
    }
#endif

    free(sc->bias);
    free(sc);
    return __OK__;
}

RET spatconv_rpi_f_destroy(SPATCONV_RPI_F sc) {

#ifdef _NEURAGHE_
    if(sc->kern_s[2] != 5 && sc->kern_s[2] != 3)
    {       
#endif

    free(sc->kernel);

#ifdef _NEURAGHE_
    }
#endif

    free(sc->bias);
    free(sc);
    return __OK__;
}

