#pragma GCC diagnostic ignored "-Wformat"

#include "spatconvtranspose.h"
//#include "channel.h"
#include "xassert.h"


#include "paramIO.h"

RET spatconvtranspose_sw_8(signed char* input, signed char* output, signed char* kernel,
    signed char* bias, SIZE in_s[3], SIZE out_s[3], SIZE kern_s[2], SIZE dil[2], SIZE pad[2],
    SIZE stride[2], bool activate, int ncol, int qf) {


  return ERR;
}

RET spatconvtranspose_sw_16(short int* input, short int* output, short int* kernel,
    short int* bias, SIZE in_s[3], SIZE out_s[3], SIZE kern_s[2], SIZE dil[2], SIZE pad[2],
    SIZE stride[2], bool activate, int ncol, int qf) {

  return ERR;
}

RET spatconvtranspose_forward_wrap(SPATCONVTRANSPOSE sc){

  return spatconvtranspose_forward_sw(sc, sc->input, sc->output, sc->in_s, sc->out_s, sc->stride, sc->pad, sc->activate, _N_COL_, sc->qf, sc->precision8);

}

RET spatconvtranspose_forward_sw(SPATCONVTRANSPOSE sc, DATA* input, DATA* output, SIZE in_s[3],
                        SIZE out_s[3], SIZE stride[2], SIZE pad[2], bool activate, int ncol, int qf, int precision8) {


    ASSERT(in_s[0] == sc->pin, "%s", "input planes do not match");
    ASSERT(out_s[0] == sc->pout, "%s", "output planes do not match");

    SIZE* kern_s = &sc->kern_s[0];
    SIZE* dil    = &sc->dil[0];

	int complexity = out_s[0] * in_s[0] * kern_s[2] * kern_s[3] * out_s[1] * out_s[2];

    _rprintf_("[%dx%d] software spatial convolution:\n", kern_s[2], kern_s[3]);
    _rprintf_("\t%lu to %lu\n", in_s[0], out_s[0]);
    _rprintf_("\t%lux%lu to %lux%lu\n", in_s[1], in_s[2], out_s[1], out_s[2]);

if(precision8){
	_tcreate_(time);
	spatconvtranspose_sw_8((signed char *)input, (signed char *)output, (signed char *)sc->kernel, (signed char *)sc->bias, in_s, out_s, &kern_s[2], &dil[0], pad, stride, activate, ncol, qf);
	_tprintf_("\tConv_exec 8: %5.3f ms,  time/complexity: %5.6f us,  GMACs: %5.2f\n", (get_wall_time()-time)/1000, (get_wall_time()-time)/complexity, (double)complexity/(get_wall_time()-time)/1000);
}

else{
	_tcreate_(time);
	spatconvtranspose_sw_16((short int *)input, (short int *)output, (short int *)sc->kernel, (short int *)sc->bias, in_s, out_s, &kern_s[2], &dil[0], pad, stride, activate, ncol, qf);
	_tprintf_("\tConv_exec 16: %5.3f ms,  time/complexity: %5.6f us,  GMACs: %5.2f\n", (get_wall_time()-time)/1000, (get_wall_time()-time)/complexity, (double)complexity/(get_wall_time()-time)/1000);
}

    _dprintf_("\tInput Checksum: %lld\n", checksum(input, in_s[0]*in_s[1]*in_s[2]));
    _dprintf_("\tWeights Checksum: %lld\n", checksum(sc->kernel, in_s[0]*out_s[0]*kern_s[2]*kern_s[2]));
    
    return __OK__;
}



SPATCONVTRANSPOSE spatconvtranspose_create(void) {
    SPATCONVTRANSPOSE sc = (SPATCONVTRANSPOSE) calloc(1, sizeof(struct SpatconvTranspose));
    return sc;
}


RET spatconvtranspose_destroy(SPATCONVTRANSPOSE sc) {

#ifdef _NEURAGHE_
    if(sc->kern_s[2] != 5 && sc->kern_s[2] != 3)
    {       
#endif

    free(sc->kernel);

#ifdef _NEURAGHE_
    }
#endif

    free(sc->bias);
    free(sc);
    return __OK__;
}

