#include "source.h"

#include "app/imageUtils.h"

#define MIN(a,b) (((a)<(b))?(a):(b))

RET source_forward(NAME filename, DATA* output, SIZE out_s[3], int qf, float norm) {

	SIZE ndata = out_s[0] * out_s[2] * out_s[2];
	ITER i = 0;

	float* a = NULL;
	float* b = NULL;
	VARSIZE a_size[3] = { 0 };
	VARSIZE b_size[3] = { 0 };

	_tcreate_(time);
	
	
	FILE* fp;
	int size;
	do {
	  fp = fopen( filename,"r" );
	  size=0;
	  if (NULL != fp) {
            fseek (fp, 0, SEEK_END);
            size = ftell(fp);

          }
          fclose(fp);
	} while (0 == size);
	
	

	loadImage(filename, a_size, &a, norm);



	if(out_s[0] != a_size[0]) {
	  printf ("Number of input channels doesn't match expected input channels!\n");
	  printf ("\t Given: %d Expected: %d\n", a_size[0], out_s[0]);
}
_dprintf_("Interpolation\n");
	interpol_scale(&a, &b, a_size, b_size, out_s[2]);
//	printf ("asize %d %d %d\n",a_size[0],a_size[1],a_size[2]);
//	printf ("bsize %d %d %d\n",b_size[0],b_size[1],b_size[2]);

_dprintf_("Normalization\n");
	normalize(&b, &a, b_size);
//	printf ("asize %d %d %d\n",a_size[0],a_size[1],a_size[2]);
//	printf ("bsize %d %d %d\n",b_size[0],b_size[1],b_size[2]);

	assignSize(a_size, b_size, 3);
//	printf ("asize %d %d %d\n",a_size[0],a_size[1],a_size[2]);
//	printf ("bsize %d %d %d\n",b_size[0],b_size[1],b_size[2]);

_dprintf_("Crop\n");
	crop(&a, &b, a_size, b_size, out_s[2]);
//	printf ("asize %d %d %d\n",a_size[0],a_size[1],a_size[2]);
//	printf ("bsize %d %d %d\n",b_size[0],b_size[1],b_size[2]);
	
//	}
//	else{
//	  printf ("ssssssssss");
//	  exit(1);
//		b = a;
//		}
if(out_s[0] < a_size[0]) {
  printf ("Gray scale conversion\n");
  int px_per_ch = a_size[1]*a_size[2];
  
  for (i = 0; i < px_per_ch; i++) {
    float gray= (b[i] + b[i+px_per_ch] + b[i+px_per_ch*2])/3;
    b[i]=gray;
  }
}

if(out_s[0] > a_size[0]) {
  printf ("Channel 0 is goig to be copied in the other channels\n");
  int px_per_ch = a_size[1]*a_size[2];
  
	float* c = NULL;
	c= (float*) calloc(out_s[0] * out_s[1] * out_s[2], sizeof(float));
  
  for (i = 0; i < px_per_ch; i++) {
    c[i]             = b[i];
    c[i+px_per_ch]   = b[i];
    c[i+px_per_ch*2] = b[i];
  }
  
  b=c;
}

	if (sizeof(DATA) != sizeof(float)) {
		for (i = 0; i < ndata; i++) {
			output[i] = (DATA)(FLOAT2FIXED(b[i], qf));
			//printf("output[%d] --> %+3.10f --> fixed: %d  -->  %+3.10f\n", i, b[i], output[i], FIXED2FLOAT(output[i], qf)); 
		}
	} else {
		memcpy(output, b, ndata * sizeof(float));
	}

//exit(8);

	free(a);
	
	free(b);

	_tprintf_("\tsource_forward: %5.3f ms\n", (get_wall_time()-time)/1000);
	return __OK__;
}


RET source_forward_letterbox(NAME filename, DATA* output, SIZE out_s[3], int qf, float norm) {
  // CHW
  
  
	SIZE ndata = out_s[0] * out_s[2] * out_s[2];
	ITER i = 0;

	float* a = NULL;
	float* b = NULL;
	VARSIZE a_size[3] = { 0 };
	VARSIZE b_size[3] = { 0 };

	_tcreate_(time);
	
	
	FILE* fp;
	int size;
	do {
	  fp = fopen( filename,"r" );
	  size=0;
	  if (NULL != fp) {
            fseek (fp, 0, SEEK_END);
            size = ftell(fp);

          }
          fclose(fp);
	} while (0 == size);
	
	

	loadImage(filename, a_size, &a, norm);



	
	interpol_keep_format(&a, &b, a_size, b_size, out_s[2]);
	
	a_size[0]=out_s[0];
	a_size[1]=out_s[1];
	a_size[2]=out_s[2];

	letterbox_image (b, a, b_size, a_size, 128.0/norm);



	if (sizeof(DATA) != sizeof(float)) {
		for (i = 0; i < ndata; i++) {
			output[i] = (DATA)(FLOAT2FIXED(a[i], qf));
//			printf("output[%d] --> %+3.10f --> fixed: %d  -->  %+3.10f\n", i, b[i], output[i], FIXED2FLOAT(output[i])); 
		}
	} else {
		memcpy(output, b, ndata * sizeof(float));
	}

	//free(a);
	//free(b);

	_tprintf_("\tsource_forward: %5.3f ms\n", (get_wall_time()-time)/1000);
	return __OK__;
}

