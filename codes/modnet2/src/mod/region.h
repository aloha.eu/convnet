#include "types.h"


typedef struct{
    float x, y, w, h;
} box;

typedef struct detection{
    box bbox;
    int classes;
    float *prob;
    float *mask;
    float objectness;
    int sort_class;
} detection;



struct Region {
	float* input;
	float* output;
	float* anchors;
	char **classes;
	
	int num_classes;
	int num_boxes;
	int h_grid;
	int w_grid;
	
	int relative;
	
	float confidence_trsh;
	float overlap_trsh;
};

typedef struct Region* REGION;



REGION region_create(void);
RET region_forward_wrap(REGION rg);


RET region_forward(float *input, 	//output of last convolutional layer
                  int l_outputs,	 //output size: SxSxn_boxesx(classes+coords+confidence)
                  int n_boxes,	 //num of boxes in each grid cell
                  int l_h, 	//grid cell height
                  int l_w, 	//grid cell width
                  int l_classes, 	//num of classes
                  float *anchors, 		//anchors
                  float thresh,		//confidence threshold
                  int relative, 	//if 1 coords are relative to the cell w and h
                  float nms,		//overlap threshold
                  char ** classes);
                  
                  
