#ifndef RELU_H
#define RELU_H

#include "types.h"

struct Relu {
	DATA* input;
	DATA* output;
	DATA alpha; // if alpha ==0 -> standard RELU, if alpha > 0 -> Leaky RELU
	
	VARSIZE size[3];
	
	int qf;
};

typedef struct Relu* RELU;

RELU relu_create(void);
RET relu_forward_wrap(RELU r);
RET relu_forward(DATA* input, DATA* output, SIZE size[3], DATA alpha, int qf);
RET relu_f32_forward(float* input, float* output, SIZE size[3]);
RET relu_fp16_forward(int16_t* input, int16_t* output, SIZE size[3]);

#endif
