#ifndef SOURCE_H
#define SOURCE_H

#include "types.h"

RET source_forward(NAME filename, DATA* output, SIZE out_s[3], int qf, float norm=255);
RET source_forward_letterbox(NAME filename, DATA* output, SIZE out_s[3], int qf, float norm=255);
#endif
