#ifndef IDENTITY_H
#define IDENTITY_H

#include "types.h"

RET identity_forward(DATA* input, DATA* output, SIZE size[3]);

#endif
