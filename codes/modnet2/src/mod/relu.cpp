#include "relu.h"

#include "paramIO.h"
#ifdef _ARM_
#include "arm_neon.h"
#define NEON_BLOCK 4
#endif


RELU relu_create(void) {
    RELU r = (RELU) calloc(1, sizeof(struct Relu));
    return r;
}

RET relu_forward_wrap(RELU r) {
   // printf ("ffffyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy\n");
	return relu_forward(r->input, r->output, r->size, r->alpha, r->qf);
}


static inline RET relu_forward_sw(DATA* input, DATA* output, SIZE size[3], DATA alpha, int qf) {
	ITER i = 0;
	SIZE memsize = size[0]*size[1]*size[2];
	//printf("memsize: %d\n",memsize);
	for(i = 0; i < memsize; i++) {
		DATA v = input[i];
		
#ifdef _FIXED_
		v = v > 0 ? v : (v*alpha)>>qf;
#else
		v = v > 0 ? v : v*alpha;
#endif

		//printf("v %d\n", v);
		output[i] = v;
	}
	return __OK__;
}

//FIXME use NEON HERE
static inline RET relu_f32_forward_sw(float* input, float* output, SIZE size[3]) {
	ITER i = 0;
	SIZE memsize = size[0]*size[1]*size[2];
	for(i = 0; i < memsize; i++) {
		float v = input[i];
		v = v > 0 ? v : 0;
		output[i] = v;
	}
	return __OK__;
}

//FIXME use NEON HERE
static inline RET relu_fp16_forward_sw(int16_t* input, int16_t* output, SIZE size[3]) {
	ITER i = 0;
	SIZE memsize = size[0]*size[1]*size[2];
	for(i = 0; i < memsize; i++) {
		int16_t v = input[i];
		v = v > 0 ? v : 0;
		output[i] = v;
	}
	return __OK__;
}

RET relu_forward(DATA* input, DATA* output, SIZE size[3], DATA alpha, int qf) {
//   printf("ttythgfhnjfghjgkj\n");

  _dprintf_(" software relu. alpha: %d\n", alpha);

	_tcreate_(time);	
	relu_forward_sw(input, output, size, alpha, qf);
	_tprintf_("ReLU time: %5.3f ms\n", (get_wall_time()-time)/1000);

    _dprintf_("\tInput Checksum: %lld\n", checksum(input, size[0]*size[1]*size[2]));
    _dprintf_("\tInput Min: %d Max: %d Avg: %d\n", min(input, size[0]*size[1]*size[2]), max(input, size[0]*size[1]*size[2]), avg(input, size[0]*size[1]*size[2]));
    
    _dprintf_("\tOutput Checksum: %lld\n", checksum(output, size[0]*size[1]*size[2]));
    
	return __OK__;
}

RET relu_f32_forward(float* input, float* output, SIZE size[3]) {

	_tcreate_(time);	
	relu_f32_forward_sw(input, output, size);
	_tprintf_("ReLU time: %5.3f ms\n", (get_wall_time()-time)/1000);

	return __OK__;
}

RET relu_fp16_forward(int16_t* input, int16_t* output, SIZE size[3]) {

	_tcreate_(time);	
	relu_fp16_forward_sw(input, output, size);
	_tprintf_("ReLU time: %5.3f ms\n", (get_wall_time()-time)/1000);

	return __OK__;
}
