#ifndef SCALAR_H
#define SCALAR_H

#include "types.h"

struct Mul {
	DATA* input;
	DATA* output;
	int size;
	DATA scaler;
	
	int qf;
};

typedef struct Mul* MUL;


struct Add {
	DATA* input;
	DATA* output;
	int size;
	DATA bias;
};

typedef struct Add* ADD;

struct Sigmoid {
	DATA* input;
	DATA* output;
	int size;
	int qf;
};

typedef struct Sigmoid* SIGMOID;

ADD add_create(void);

MUL mul_create(void);

SIGMOID sigmoid_create(void);

RET add_forward(ADD a);
RET mul_forward(MUL m);
RET sigmoid_forward(SIGMOID s);

RET clip_forward(CLIP c);
RET floor_forward(FLOOR f);

#endif
