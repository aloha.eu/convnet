#!/bin/bash
set -e
bash create_project.sh -q -F -Q 8 -Y -T plainC yolo yolo_test/tiny_yolov2.onnx
sed -i 's/\/\*set a nomalization value\*\//1/g' tests/yolo/src/yolo.cpp
sed -i 's/\/\*input_type here\*\//3/g' tests/yolo/src/yolo.cpp
make clean all FIXED=1 TEST=tests/yolo/
./build/tests/yolo/main -c build/tests/yolo/weights -i yolo_test/sample_images/
python3 drawboxes.py results.txt yolo_test/sample_images/

