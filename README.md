# NEURAGHE Software Framework
This repository contains the sofware source code for DL application targeted on the NEURAGHE platform.

The tests can be ran in your PC or workstation. We provvide also support the the development boards Xilinx ZC706 or ZEDBOARD running Linux (tested with Ubuntu and Xillinux).  
To use all the features of the makefile we suggest to connect the board to your LAN.  

## Dependencies
### arm compiler
ARM cross compilers must be in your current $PATH: run the command ```arm-linux-gnueabihf``` to check it out.

### ONNXparser
The code autogeneration from ONNX uses the ONNXparser. Clone it from the repository:  
```bash
cd tools
git clone git@gitlab.com:aloha.eu/onnxparser.git
```

### 3rd party libraries
The following 3rd party libraries may be used:  
* GSL - GNU Scientific Library  
* Independent JPEG Group  

To install them go to `codes/3rdparty` folder and run `setup-all.sh`.  
This downloads the dependencies and compiles statically linkable libraries.  

## TL;DR
Just run:  
```
source sourceme.sh
export PATH=$PATH:./onnxparser
```
and then run the scripts below or the single commands listed.  
### KWS
#### General Purpose CPU

`bash kws_test.sh`  
or  
```bash
bash create_project.sh -T plainC kws_app examples/kws/kws_trained_90perc-accu_10classes.onnx
sed -i 's/\/\*set a nomalization value\*\//1/g' tests/kws_app/src/kws_app.cpp
sed -i 's/\/\*input_type here\*\//0/g' tests/kws_app/src/kws_app.cpp
make clean all FIXED=1 DEBUG=1 TEST=tests/kws_app
./build/tests/kws_app/main -c build/tests/kws_app/weights -i examples/kws/kws10_example_spectrograms
python3 tools/check_results.py results.txt examples/kws/kws10_example_spectrograms_label.txt
```
#### Accelerated with NEURAghe on ZEDBOARD
We assume you have an ARM compiler and the required files for NEURAghe.
On your local machine:  
```bash
bash create_project.sh -T NEURAghe --nRow 2 --nCol 2 kws_app examples/kws/kws_trained_90perc-accu_10classes.onnx
sed -i 's/\/\*set a nomalization value\*\//1/g' tests/kws_app/src/kws_app.cpp
sed -i 's/\/\*input_type here\*\//0/g' tests/kws_app/src/kws_app.cpp
make clean all FIXED=1 NEURAGHE=1 TEST=tests/kws_app
```
On the ZEDBOARD, after you copied the application folder:  
```bash
./main -c weights -i /home/marco/kws_app/example/kws10_example_spectrograms -b zynq/neu1.9_zed_1020_jan19.bin
```

Back to your local machine:  
```bash
python3 tools/check_results.py results.txt examples/kws/kws10_example_spectrograms_label.txt
```


Because of the small number of samples, the accuracy will be 80% only.


### VGG
Clone the repository containing the onnx and some test image extracted from ImageNet dataset  
```bash
git clone git@gitlab.com:aloha.eu/vgg_test.git
```

The onnx is the one provided [here](https://github.com/onnx/models/tree/master/vision/classification/vgg)

`bash vgg_test.sh`  
or  
```bash
bash create_project.sh -F -Q 8 -T plainC vgg_app vgg_test/vgg16-bn.onnx
sed -i 's/\/\*set a nomalization value\*\//1/g' tests/vgg_app/src/vgg_app.cpp
sed -i 's/\/\*input_type here\*\//2/g' tests/vgg_app/src/vgg_app.cpp
make clean all FIXED=1 DEBUG=1 TEST=tests/vgg_app
./build/tests/vgg_app/main -c build/tests/vgg_app/weights -i vgg_test/subset_imagenet
python3 tools/check_results.py results.txt vgg_test/imagenet_labels.txt
```
Expected accuracy with different values of QF (number of fractional bits)  
```
qf8  -> 71.43%
qf9  -> 71.43%
qf10 -> 64.29%
qf11 -> 57.14%
qf12 ->  7.14%
```

Differences with the KWS example:
* the input images are JPEG preprocessed inside the application. The [ImageNet normalization](https://github.com/onnx/models/tree/master/vision/classification/vgg#preprocessing) is applied
* The folding of the Batch Normalization operator is performed
* The conversion and the execution of the net are much slower 

### YOLO
Tiny YOLO has been tested.
Clone the repository containing the onnx and some test image  
```
git clone git@gitlab.com:aloha.eu/yolo_test.git
```
The onnx is the one provided [here](https://github.com/onnx/models/tree/master/vision/object_detection_segmentation/tiny_yolov2)  

`bash yolo_test.sh`  
or  
```bash
bash create_project.sh -F -Q 8 -Y -T plainC yolo yolo_test/tiny_yolov2.onnx
sed -i 's/\/\*set a nomalization value\*\//1/g' tests/yolo/src/yolo.cpp
sed -i 's/\/\*input_type here\*\//3/g' tests/yolo/src/yolo.cpp
make clean all FIXED=1 DEBUG=1 TEST=tests/yolo/
./build/tests/yolo/main -c build/tests/yolo/weights -i yolo_test/sample_images/
python3 drawboxes.py
```
You'll find the images with impressed bounding boxes in the folder `boxes`


### MobileNet
We tested a custom implementation of MobileNet. It has been trained to classify animals and people.  

`bash mobilenet_test.sh`  
or  
```bash
bash create_project.sh -Q 6 -T plainC mobile ./examples/mobile_net.onnx
sed -i 's/\/\*set a nomalization value\*\//1/g' tests/mobile/src/mobile.cpp
sed -i 's/\/\*input_type here\*\//2/g' tests/mobile/src/mobile.cpp
make clean all FIXED=1 DEBUG=1 TEST=tests/mobile
./build/tests/mobile/main -c build/tests/mobile/weights -i ./examples/people_vs_animals
python3 tools/check_results.py results.txt ./examples/people_vs_animals.txt
```
You should expect an accuracy over 80%.

### InceptionNet
We tested a custom implementation of InceptionNet. It has been trained to classify animals and people.  

`bash inceptionnet_test.sh`  
or  
```bash
bash create_project.sh -Q 6 -T plainC inception ./examples/inception_net.onnx
sed -i 's/\/\*set a nomalization value\*\//1/g' tests/inception/src/inception.cpp
sed -i 's/\/\*input_type here\*\//2/g' tests/inception/src/inception.cpp
make clean all FIXED=1 DEBUG=1 TEST=tests/inception
./build/tests/inception/main -c build/tests/inception/weights -i ./examples/people_vs_animals
python3 tools/check_results.py results.txt ./examples/people_vs_animals.txt
```
You should expect an accuracy over 95%.

## Project structure
Here is a quick overview over the structure (tree -L 2 -d):  
```
├── codes (ResNet-18 application code)
│   ├── 3rdparty (libraries w/ cross compilator build scripts and prebuilt versions)
│   └── modnet2 (the current codebase)
├── tests (contains all the applications to test and use the architecture)
│   ├── conv_test (this application is intended to run a set of conv layers, measure preformance and debug the architecture)
│   └── ... (a lot of them are outdated)
└── zynq (contains the FPGA bistream for Neuraghe)
```
## Creating a new application

**Refere to the TL;DR to use the automatic generation from onnx**

You can create a new test application simply doing copy and paste of the helloworld application. The folder and the two source files must have the same name.  
The user application is embedded in a main application and is composed by two functions:  
**cnnMainInit** initalizes the application loading the static data like weights and biases  
**cnnMain** is the core function and should implement the whole net. It takes the input images and returns the result data. This function is run in a loop.  

## Compiling and running on the zynq platform

To compile the application, in the root directory run the command:  
```bash
make clean all
```
Then you can send the application to your board via ssh:  
```bash
make load NEURAGHE_BOARD_ADDR=user@hostname NEURAGHE_INSTALL_DIR=<install-directory> 
```

### Environment variables

The makefile uses a set of environment variables to enable some features or switch among debug modes.  
You can set them only once in a bash script, here called sourceme.sh or append them in the make call like in the above command.  

The available env vars and their default values:
```
TIME?= 0      # enables the performance measures and the realted prints 
DEBUG?= 0     # enables the debug prints
RELEASE?= 0   # if RELEASE=1 disables all the debug prints
FIXED?= 0     # to be set to 1 when using neuraghe
NEURAGHE?= 0  # to be set to 1 to use neuraghe
CI?= 0        # enables the print of the classification results
US?= 0        # if set to 1 changes the compilter to aarch64-linux-gnu-gcc which supports the Ultrascale's 64bit architecture
```



On the zynq board, once the FPGA is configured, go into `bin` and execute the main application (it needs root privileges):
```
sudo ./main <path-to-weights> <input-image>
```
note: for the `tigercat.jpeg` image there is a results comparison at the end of the computation.<br>

To enable debug prints, recompile the project with `DEBUG` flag enabled:
```
make clean all DEBUG=1
```
Then you can reload the the executable with:
```
make load-main NEURAGHE_BOARD_ADDR=user@hostname NEURAGHE_INSTALL_DIR=<install-directory>
```
To run the whole application on the ARM, and emulate the Neuraghe behaviour in software, disable the `NEURAGHE` flag and recompile the project:
```
make clean all NEURAGHE=0
```
If you want to use weights with different number of bits for the fractional part remember to set che correct value inside `codes\modnet2\src\net\types.h` (QF value).<br>

You can also run the whole application in floating-point precision (use double precision weights) disabling the `FIXED` flag at build time:
```
make clean all NEURAGHE=0 FIXED=0		(for ARM architecture)
make clean all NEURAGHE=0 FIXED=0 ARM=0 	(for x86 architecture)
```

## Tested on
VGG https://github.com/onnx/models/tree/master/vision/classification/vgg
Tiny Yolo v2 (onnx version 1.3) https://github.com/onnx/models/tree/master/vision/object_detection_segmentation/tiny_yolov2
A lot of KWS networks generated by ALOHA


