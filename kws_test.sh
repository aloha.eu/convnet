#!/bin/bash
set -e
bash create_project.sh -q -T plainC kws_app examples/kws/kws_trained_90perc-accu_10classes.onnx
sed -i 's/\/\*set a nomalization value\*\//1/g' tests/kws_app/src/kws_app.cpp
sed -i 's/\/\*input_type here\*\//0/g' tests/kws_app/src/kws_app.cpp
make clean all FIXED=1 TEST=tests/kws_app
./build/tests/kws_app/main -c build/tests/kws_app/weights -i examples/kws/kws10_example_spectrograms
python3 tools/check_results.py results.txt examples/kws/kws10_example_spectrograms_label.txt

