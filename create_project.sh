#!/bin/sh

TARGETS="pytorch, plainC, NEURAghe, CMSIS, ARMCL, CLKAM"

# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Usage
usage="

$(basename "$0") [-h][-q][-s][-N][-F][-Y][-S][-T target][-t path][-O path][--nRow rows][--nCol cols] <project_name> <onnx_path> -- create a C project starting from an ONNX\n

where:
    -h -? --help   show this help text
    -Q --qf        number of bits for fractional part in fixed point representation. 0 if the model is already quantized
    -s             use smart allocation of the memory
    -N             do Not extract weights
    -F             perform the BatchNormalization folding
    -Y             the net is yolo-like so add a region layer
    -S             add a softmax layer
    -T             specify the target architecture: $TARGETS. Default is plainC
    -t             specify a different template folder. Default is tests/template/
    -O             specify a different output folder. Default is tests/
    -q             quiet mode, do not open dialog box/
    --nRow         number of Rows of the NEURAghe SoP matrix/
    --nCol         number of Columns of the NEURAghe SoP matrix"

# Initialize our own variables:
output_file=""
verbose=0
extract_weights=1
smart_allocation=0
yolo_like=0
softmax=0
folding=0
TEMPLATE="$(pwd)/tests/template"
TARGET="plainC"
output_folder="$(pwd)/tests"
onnxparser_args=""
quiet=0
qf=8
nRow=0
nCol=0

while :; do
    case "$1" in
    -h|-\?--help)
        echo "$usage"
        exit 0
        ;;
    -Q|qf)  qf=$2
         onnxparser_args=$onnxparser_args"-Q $2 "
        shift
        ;;
    -N)  extract_weights=0
         onnxparser_args=$onnxparser_args"-D "
        ;;
    -F)  folding=1
         onnxparser_args=$onnxparser_args"-F "
        ;;
    -S)  softmax=1
         onnxparser_args=$onnxparser_args"-S "
        ;;
    -Y)  yolo_like=1
         onnxparser_args=$onnxparser_args"-Y "
        ;;
    -s)  smart_allocation=1
         onnxparser_args=$onnxparser_args"-SA "
        ;;
    -q)  quiet=1
        ;;
    -T)  TARGET=$2
        shift
        ;;
    --nRow) nRow=$2
         onnxparser_args=$onnxparser_args"--nRow $2 "
        shift
        ;;
    --nCol) nCol=$2
         onnxparser_args=$onnxparser_args"--nCol $2 "
        shift
        ;;
    -t)  TEMPLATE=$2
        shift
        ;;
    -O)  output_folder=$2
        shift
        ;;
    --)              # End of all options.
        shift
        break
        ;;
    *)               # Default case: No more options, so break out of the loop.
        break
    esac
    shift
done

if [ "$#" -ne 2 ]; then
  echo "Wrong number of mandatory arguments: $@"
  echo "$usage"
  exit 1
fi

if ! [[ "$TARGETS" == *"$TARGET"* ]]; then
  echo "Wrong target. It must be: $TARGETS"
  echo "$usage"
  exit 1
fi

if [ "$TARGET" = "NEURAghe" ]; then
  if [ "$nRow" -eq 0 ]; then
    echo "You must define nRow and nCol to create a NEURAghe project"
    echo "$usage"
    exit 1
  fi
  if [ "$nCol" -eq 0 ]; then
    echo "You must define nRow and nCol to create a NEURAghe project"
    echo "$usage"
    exit 1
  fi
fi
app_name=$1
onnx_path=$2

echo "appname:       $app_name"
echo "onnx_path:     $onnx_path"


echo "TEMPLATE path: $TEMPLATE"
echo "output folder: $output_folder"

if ! command -v onnxparser.py>/dev/null; then
  printf "ERROR: You must include the onnxparser folder in the PATH:\n\t export PATH=\$PATH:path/to/onnxparser\n";
  exit 1
fi
if ! command -v post_training.py>/dev/null; then
  printf "ERROR: You must include the post training folder in the PATH:\n\t export PATH=\$PATH:path/to/post_training\n";
  exit 1
fi

if [ ! -f "$TEMPLATE/src/template.cpp" ]; then
    echo "$TEMPLATE/src/template.cpp does not exist"
    exit 1
fi
if [ ! -f "$TEMPLATE/src/template.h" ]; then
    echo "$TEMPLATE/src/template.h does not exist"
    exit 1
fi

printf "Creating folder $output_folder/$app_name/src ..."
mkdir -p $output_folder/$app_name/src
printf "\e[32m done\e[39m\n"

printf "Creating $app_name.cpp ..."
sed "s/{template}/$app_name/g" $TEMPLATE/src/template.cpp > $output_folder/$app_name/src/$app_name.cpp
printf "\e[32m done\e[39m\n"

printf "Creating $app_name.h ..."
sed "s/{template}/$app_name/g" $TEMPLATE/src/template.h > $output_folder/$app_name/src/$app_name.h
printf "\e[32m done\e[39m\n"

printf "Creating folder $output_folder/$app_name/Imgs ..."
mkdir -p $output_folder/$app_name/Imgs
#cp -r $TEMPLATE/Imgs/* $output_folder/$app_name/Imgs/
printf "\e[32m done\e[39m\n"


printf "Creating folder $output_folder/$app_name/weights ..."
mkdir -p $output_folder/$app_name/weights
mkdir -p $output_folder/$app_name/weights/SW
printf "\e[32m done\e[39m\n"


printf "Processing model ...\n"

onnxparser.py $onnx_path $app_name $onnxparser_args -T $TARGET -P $output_folder/$app_name/src/ || exit 1

if [ "$extract_weights" -eq 1 ]; then
  cd temp_weights_$app_name
  cp conv.json ../
  #find . -name "*.bin" -exec bash -c "cp '{}' $output_folder/$app_name/weights/" \;
  #cd ..
  #printf "\e[32mdone\e[39m\n"
  #rm -r ./temp_weights_$app_name
  printf "Copying the parameters files of the layers different than Convolutions ...\n"
  find . ! -name "*Conv*.bin" -a -name "*.bin" -exec bash -c "cp '{}' $output_folder/$app_name/weights/" \;
  printf "Copying the parameters files of the Convolutional layer in the weights/SW folder ...\n"
  find . -name "*Conv*.bin" -a -name "*.bin" -exec bash -c "cp '{}' $output_folder/$app_name/weights/SW" \;
  
  if [ "$TARGET" = "CMSIS" ]; then
    printf "Copying the parameters files of the Convolutional layer in the weights/CMSIS folder ...\n"
    mkdir -p $output_folder/$app_name/weights/CMSIS
    find . -name "*Conv*.h" -o -name "*.h" -exec bash -c "cp '{}' $output_folder/$app_name/weights/CMSIS" \;
  fi
  
  if [ "$TARGET" = "ARMCL" ]; then
    printf "Copying the parameters files of the Convolutional layer in the weights/ARMCL folder ...\n"
    mkdir -p $output_folder/$app_name/weights/ARMCL
    find . -name "*.npy" -exec bash -c "cp '{}' $output_folder/$app_name/weights/ARMCL" \;
  fi
  cd ..
  printf "\e[32m done\e[39m\n"
  if [ "$TARGET" = "NEURAghe" ]; then
    printf "Weights processing (post-training) ...\n"
  
    cd ./temp_weights_$app_name
    sed -e "s/QF_o *= *[0-9]*/QF_o  =  $qf/; s/NROW *= *[0-9]*/NROW  =  $nRow/; s/NCOL *= *[0-9]*/NCOL  =  $nCol/" ../post_training_settings_template.txt > post_training_settings.txt
    post_training.py ./post_training_settings.txt || exit 1
    cp -r ./pt/* $output_folder/$app_name/weights
    cd ..
    printf "\e[32mdone\e[39m\n"
  fi
  rm -r ./temp_weights_$app_name
  rm -r conv.json
else
  printf "\e[33mSkipping weights!\e[39m\n"
fi


printf "\n\e[32mProject successfully created!\e[39m\n\n"
if [ "$quiet" -eq 0 ]; then
  printf "You can compile (for your local machine) the new project with the command: \n The following commands work only if the application is in the test folder\n\n"
  printf "$ \e[96mmake clean all FIXED=1 TEST=tests/$app_name\e[39m\n"
  printf "To enable debug prints: \n"
  printf "$ \e[96mmake clean all FIXED=1 DEBUG=1 TEST=tests/$app_name\e[39m\n\n"
  printf "To cross-compile for ARM cores: \n"
  printf "$ \e[96mmake clean all FIXED=1 ARM=1 TEST=tests/$app_name\e[39m\n\n"
  if [ "$TARGET" = "NEURAghe" ]; then
    printf "To cross-compile for ARM cores with NEURAghe acceleration: \n"
    printf "$ \e[96mmake clean all FIXED=1 ARM=1 NEURAGHE=1 TEST=tests/$app_name\e[39m\n\n"
  fi

  printf "Please, refer to the documentation for other options.\n"


  xmessage -default okay -center - "Project created!"
fi




