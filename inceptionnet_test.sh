#!/bin/bash
set -e
bash create_project.sh -q -Q 6 -T plainC inception ./examples/inception_net.onnx
sed -i 's/\/\*set a nomalization value\*\//1/g' tests/inception/src/inception.cpp
sed -i 's/\/\*input_type here\*\//2/g' tests/inception/src/inception.cpp
make clean all FIXED=1 TEST=tests/inception
./build/tests/inception/main -c build/tests/inception/weights -i ./examples/people_vs_animals
python3 tools/check_results.py results.txt ./examples/people_vs_animals.txt
