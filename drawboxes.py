import cv2, os, argparse
import numpy as np
lcolors =  [[255, 0,  128],
                         [0,   255, 255],
                         [128, 128, 0],
                         [0,   128, 128],
                         [128, 0,   255],
                         [0,   0,   0],
                         [0,   255, 128],
                         [0,   255, 0],
                         [128, 255, 255],
                         [0,   0,   128],
                         [128, 0,   0],
                         [128, 0,   128],
                         [0,   128, 0],
                         [128, 255, 0],
                         [128, 128, 255],
                         [0,   128, 255],
                         [128, 128, 128],
                         [0,   0,   255],
                         [128, 255, 128],
                         [255, 0,   0]
                         ]
                         
parser = argparse.ArgumentParser()
parser.add_argument('results', default=None, type=str, help =" results file")
parser.add_argument('images_path', default=None, type=str, help =" images path")
args = parser.parse_args()
results         = args.results
images_path         = args.images_path

                         
img_folder = images_path
img_output = "boxes"
os.makedirs(img_output, exist_ok=True)
with open(results) as f:
  lines=f.readlines()

classes = []

images = {}
for l in lines:
  if ".jpg" in l:
    img = l.strip()
    images[img]=[]
  if "," in l:
    box=l.split(",")
    if not box[0].strip() in classes:
      classes.append(box[0].strip()) 
    box = [box[0].strip()]+ [float(a.strip()) for a in box[1:]]
    images[img].append(box)

print (images)
for i in images:
  
  img = cv2.imread(os.path.join(img_folder,i))
  img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
  
  
  h, w, _ = img.shape
  dim_diff = np.abs(h - w)
  pad1, pad2 = dim_diff // 2, dim_diff - dim_diff // 2
 
  # Determine padding
  pad = (0, 0, pad1, pad2) if h <= w else (pad1, pad2, 0, 0)

  # Add padding
  img = cv2.copyMakeBorder(img, pad[2], pad[3], pad[0], pad[1], cv2.BORDER_CONSTANT, value=[128,128,128])
  
  
  img = cv2.resize(img, (416, 416), interpolation=cv2.INTER_LINEAR)
  
  for b in images[i]:
    bb = [b[2],b[3],b[4],b[5]]
    bb = [int(a*416) for a in bb]
    
    
    
    bb[0] = bb[0]-(bb[2]//2)
    bb[1] = bb[1]-(bb[3]//2)
    bb[2] = bb[0]+bb[2]
    bb[3] = bb[1]+bb[3]
    
    bb = [415 if a>415 else a  for a in bb]
    classl = classes.index(b[0])
    print (bb)
    print (img.shape)
    if bb[0]<bb[2] and bb[1]<bb[3]:
          for y in range(bb[1],bb[3]):
              img[y][bb[0]][0] = lcolors[classl][0]
              img[y][bb[0]][1] = lcolors[classl][1]
              img[y][bb[0]][2] = lcolors[classl][2]
              img[y][bb[2]][0] = lcolors[classl][0]
              img[y][bb[2]][1] = lcolors[classl][1]
              img[y][bb[2]][2] = lcolors[classl][2]

          for x in range(bb[0],bb[2]):
              img[bb[1]][x][0] = lcolors[classl][0]
              img[bb[1]][x][1] = lcolors[classl][1]
              img[bb[1]][x][2] = lcolors[classl][2]
              img[bb[3]][x][0] = lcolors[classl][0]
              img[bb[3]][x][1] = lcolors[classl][1]
              img[bb[3]][x][2] = lcolors[classl][2]
  
    cv2.putText(img, text=b[0] + " {:.1f}%".format(b[1]), org=(bb[0],bb[1]+10),
            fontFace= cv2.FONT_HERSHEY_PLAIN, fontScale=0.8, color=lcolors[classl],
            thickness=1)
  img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
  cv2.imwrite(os.path.join(img_output,i), img)
  
  
  
  
  
  
  
  
  
