#!/bin/bash
set -e
bash create_project.sh -q -F -Q 8 -T plainC vgg_app vgg_test/vgg16-bn.onnx
sed -i 's/\/\*set a nomalization value\*\//1/g' tests/vgg_app/src/vgg_app.cpp
sed -i 's/\/\*input_type here\*\//2/g' tests/vgg_app/src/vgg_app.cpp
make clean all FIXED=1 TEST=tests/vgg_app
./build/tests/vgg_app/main -c build/tests/vgg_app/weights -i vgg_test/subset_imagenet
python3 tools/check_results.py results.txt vgg_test/imagenet_labels.txt
