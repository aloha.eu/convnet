#!/bin/bash
set -e
bash create_project.sh -q -Q 6 -T plainC mobile ./examples/mobile_net.onnx
sed -i 's/\/\*set a nomalization value\*\//1/g' tests/mobile/src/mobile.cpp
sed -i 's/\/\*input_type here\*\//2/g' tests/mobile/src/mobile.cpp
make clean all FIXED=1 TEST=tests/mobile
./build/tests/mobile/main -c build/tests/mobile/weights -i ./examples/people_vs_animals
python3 tools/check_results.py results.txt ./examples/people_vs_animals.txt
