import argparse, sys, os

parser = argparse.ArgumentParser()
parser.add_argument('results', default=None, type=str, help =" results file")
parser.add_argument('labels', default=None, type=str, help =" ground thruth file")
args = parser.parse_args()
results         = args.results
labels         = args.labels

# the results file created by the test
with open(results,"r") as f_results:
  results=f_results.readlines()

#the labels file associated to the dataset in the ALOHA standard format or a list of "filename label" pairs
with open(labels,"r") as f_labels:
  labels=f_labels.readlines()
  
  
print ("\n\n\n{:^15s}{:^15s}{:^15s}{:^15s}{:^15s}".format("tot", "accuracy[%]", "file", "expected", "predicted"))
print ('----------------------------------------------------------------------------')
n=0
acc=0
for line in results:
  image, top0 = line.split(" ")
 # print (line)
  ok=0  
  for expected in labels:
    expec, lbl = expected.split(" ")
    expec = os.path.basename(expec)
    if image.split(".")[0]==expec.split(".")[0]:
      if int(lbl)==int(top0):
        ok = 1
      break
      
  
  acc = acc +ok
  n+=1
  if n==2081:
    print(expected)
    print(line)
  color='\033[44;33m' if ok else '\033[43;30m'
  print ("{}{}{:^15}{:^13.2f}{:^15}{:^15}{:^15}\033[m".format (color,'ok' if ok else '--',n, acc/n*100, image.split(".")[0][0:3] +"..."+ image.split(".")[0][-5:],lbl.replace('\n',''), top0.replace('\n','')))
  
  #print ("ok acc: {}%\n\n".format(acc/n*100) if ok else "no acc: {}%\n\n".format(acc/n*100))
  
print ("Accuracy: %.2f%%\n\n" % (acc/n*100))
